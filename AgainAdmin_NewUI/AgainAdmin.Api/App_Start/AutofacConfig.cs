﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Autofac;
using Autofac.Integration.SignalR;
using Autofac.Integration.WebApi;
using Microsoft.AspNet.SignalR;
using System.Web;
using System.Web.Http;
using AgainAdmin.ApplicationService;
using AgainAdmin.EF;
using AgainAdmin.Framework;

namespace AgainAdmin.Api.App_Start
{
    public class AutofacConfig
    {
        public static IContainer CreateContainer(HttpConfiguration config)
        {
            var builder = new ContainerBuilder();

            // Register Web API controllers.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            // Register your SignalR hubs.
            builder.RegisterHubs(Assembly.GetExecutingAssembly());

            // Register mudules
            builder.RegisterModule(new FrameworkModule());
            builder.RegisterModule(new EFModule());
            builder.RegisterModule(new ApplicationModule());
            builder.RegisterModule(new ApiModule());

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
            GlobalHost.DependencyResolver = new AutofacDependencyResolver(container);

            return container;
        }
    }
}