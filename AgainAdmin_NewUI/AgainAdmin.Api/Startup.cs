﻿using Autofac;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Http;
using AgainAdmin.Api.App_Start;
using AgainAdmin.Api.Infrastructure;
using AgainAdmin.Api.Security;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.DataHandler.Encoder;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Security;

[assembly: OwinStartup(typeof(AgainAdmin.Api.Startup))]

namespace AgainAdmin.Api
{
    public class Startup
    {
        private IContainer _container;

        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();

            // Autofac
            ConfigureAutofac(config);

            // OAuth 2.0
            ConfigureOAuthTokenGeneration(app);
            //ConfigureOAuthTokenConsumption(app);

            // SignalR
            ConfigureSignalR(app);

            // WebApi
            ConfigureWebApi(config, app);
        }

        private void ConfigureWebApi(HttpConfiguration config, IAppBuilder app)
        {
            WebApiConfig.Register(config);
            app.UseWebApi(config);
        }

        private void ConfigureAutofac(HttpConfiguration config)
        {
            _container = AutofacConfig.CreateContainer(config);
        }

        private void ConfigureOAuthTokenGeneration(IAppBuilder app)
        {
            var oAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/generatetoken"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                Provider = _container.Resolve<OAuthAuthorizationServerProvider>()
            };

            // Token Generation
            app.UseOAuthAuthorizationServer(oAuthServerOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }

        private void ConfigureSignalR(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}
