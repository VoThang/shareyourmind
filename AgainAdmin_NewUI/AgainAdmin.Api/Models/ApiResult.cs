﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgainAdmin.Api.Models
{
    public class ApiResult
    {
        public string Status { get; set; }
        public string Message { get; set; }
    }
}