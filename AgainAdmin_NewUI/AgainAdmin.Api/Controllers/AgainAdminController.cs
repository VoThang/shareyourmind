﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AgainAdmin.Api.Models;
using AgainAdmin.ApplicationService.Application;
using AgainAdmin.ApplicationService.Model;
using AgainAdmin.EF.Entity;
using AgainAdmin.Framework.Configuration;
using AgainAdmin.Framework.Logging;

namespace AgainAdmin.Api.Controllers
{
    [Authorize]
    public class AgainAdminController : ApiController
    {
        private readonly IApplicationService _applicationService;

        public AgainAdminController(IApplicationService applicationService)
        {
            _applicationService = applicationService;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ApiResult> Register(UserModel user)
        {
            var isExistEmail = await _applicationService.CheckExistEmail(user.Email);
            if (isExistEmail)
            {
                return new ApiResult()
                {
                    Status = "Fail",
                    Message = "Email already exist"
                };
            }

            await _applicationService.CreateUser(user);

            return new ApiResult()
            {
                Status = "Success",
                Message = ""
            };
        }

        [HttpPost]
        public async Task<UserModel> GetUser(UserModel user)
        {
            var task = await _applicationService.GetUserByEmail(user.Email);

            return task;
        }

        [HttpPost]
        public async Task<bool> CreateUserImage(UserImageModel userImage)
        {
            var task = await _applicationService.CreateUserImage(userImage);

            return task;
        }

        [HttpPost]
        public async Task<ApiResult> UpdateUser(UserModel user)
        {
            await _applicationService.UpdateUser(user);

            return new ApiResult()
            {
                Status = "Success",
                Message = ""
            };
        }

        [HttpPost]
        public async Task<IEnumerable<UserModel>> GetUsers()
        {
            var task = await _applicationService.GetUsers();

            return task;
        }

        [HttpPost]
        public async Task<IEnumerable<BlogModel>> GetBlogs()
        {
            var task = await _applicationService.GetBlogs();

            return task;
        }

        [HttpPost]
        public async Task<BlogModel> GetBlog(BlogModel blogModel)
        {
            var task = await _applicationService.GetBlog(blogModel.Id);

            return task;
        }

        [HttpPost]
        public async Task<ApiResult> CreateBlog(BlogModel blogModel)
        {
            await _applicationService.CreateBlog(blogModel);

            return new ApiResult()
            {
                Status = "Success",
                Message = ""
            };
        }

        [HttpPost]
        public async Task<ApiResult> UpdateBlog(BlogModel blogModel)
        {
            await _applicationService.UpdateBlog(blogModel);

            return new ApiResult()
            {
                Status = "Success",
                Message = ""
            };
        }

        [HttpPost]
        public async Task<ApiResult> RemoveBlog(BlogModel blogModel)
        {
            await _applicationService.RemoveBlog(blogModel);

            return new ApiResult()
            {
                Status = "Success",
                Message = ""
            };
        }

        [HttpPost]
        public async Task<IEnumerable<CommentModel>> GetComments(BlogModel blogModel)
        {
            var task = await _applicationService.GetComments(blogModel.Id);

            return task;
        }

        [HttpPost]
        public async Task<ApiResult> CreateComment(CommentModel commentModel)
        {
            await _applicationService.CreateComment(commentModel);

            return new ApiResult()
            {
                Status = "Success",
                Message = ""
            };
        }

        [HttpPost]
        public async Task<IEnumerable<LikesModel>> GetLikes(BlogModel blogModel)
        {
            var task = await _applicationService.GetLikes(blogModel.Id);

            return task;
        }

        [HttpPost]
        public async Task<IEnumerable<LikesModel>> CreateLikes(LikesModel likesModel)
        {
            return await _applicationService.CreateLike(likesModel);
        }
    }
}
