﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgainAdmin.Api.Security;
using Autofac;
using Microsoft.Owin.Security.OAuth;

namespace AgainAdmin.Api
{
    public class ApiModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AuthorizationServerProvider>().As<OAuthAuthorizationServerProvider>().SingleInstance();
        }
    }
}