﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using Autofac;
using Microsoft.Owin.Security.OAuth;
using System.Threading.Tasks;
using AgainAdmin.Api.Infrastructure;
using AgainAdmin.ApplicationService.Application;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace AgainAdmin.Api.Security
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        private readonly ILifetimeScope _container;

        public AuthorizationServerProvider(ILifetimeScope container)
        {
            _container = container;
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
            return Task.FromResult<object>(null);
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            using (var scope = _container.BeginLifetimeScope())
            {
                var userAppService = scope.Resolve<IApplicationService>();
                var encryptionService = scope.Resolve<IEncryptionService>();

                var passwordEncrypt = encryptionService.EncryptText(context.Password);

                var password = await userAppService.GetPasswordByEmail(context.UserName);
                if (!password.Equals(passwordEncrypt))
                {
                    context.Rejected();
                    return;
                }
            }

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            identity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));
            context.Validated(identity);
        }
    }
}