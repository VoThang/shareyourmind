﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgainAdmin.Framework.Configuration
{
    public class AppSettingFactory : IAppSettingFactory
    {
        private IAppSetting _appSettings;

        public AppSettingFactory(IAppSetting appSettings)
        {
            _appSettings = appSettings;
        }

        public IAppSetting GetSettings()
        {
            return _appSettings;
        }
    }
}