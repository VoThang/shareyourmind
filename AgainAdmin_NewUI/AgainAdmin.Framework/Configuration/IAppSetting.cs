﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgainAdmin.Framework.Configuration
{
    public interface IAppSetting
    {
        string LoggerName { get; }
    }
}