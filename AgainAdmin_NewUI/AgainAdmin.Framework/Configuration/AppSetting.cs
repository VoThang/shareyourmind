﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace AgainAdmin.Framework.Configuration
{
    public class AppSetting : IAppSetting
    {
        private const string LOGGER_NAME = "LoggerName";

        public string LoggerName
        {
            get { return ConfigurationManager.AppSettings[LOGGER_NAME]; }
        }
    }
}