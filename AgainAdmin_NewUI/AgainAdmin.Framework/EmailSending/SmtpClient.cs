﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using AgainAdmin.Framework.Configuration;

namespace AgainAdmin.Framework.EmailSending
{
    public class SmtpClient : System.Net.Mail.SmtpClient, ISmtpClient
    {
        public SmtpClient(IAppSettingFactory appSettingFactory)
        {
            Host = appSettingFactory.GetSettings().SmtpServer;
            Port = Convert.ToInt32(appSettingFactory.GetSettings().SmtpPort);
            DeliveryMethod = SmtpDeliveryMethod.Network;
            EnableSsl = true;
            UseDefaultCredentials = false;
            Credentials = new NetworkCredential(appSettingFactory.GetSettings().SmtpUser,
                                                        appSettingFactory.GetSettings().SmtpPass);
        }

        public new Task Send(MailMessage message)
        {
            return SendMailAsync(message);
        }
    }
}