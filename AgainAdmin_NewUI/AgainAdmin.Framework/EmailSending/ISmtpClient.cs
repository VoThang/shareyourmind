﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;

namespace AgainAdmin.Framework.EmailSending
{
    public interface ISmtpClient
    {
        Task Send(MailMessage message);
    }
}