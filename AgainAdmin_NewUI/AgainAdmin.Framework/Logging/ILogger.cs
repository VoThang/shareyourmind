﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgainAdmin.Framework.Logging
{
    public enum LogLevel
    {
        Debug,
        Information,
        Warning,
        Error,
        Fatal
    }

    public interface ILogger
    {
        void Log(LogLevel level, string format, params object[] args);
    }
}