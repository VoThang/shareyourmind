﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgainAdmin.Framework.Configuration;
using log4net;
using log4net.Config;

namespace AgainAdmin.Framework.Logging
{
    public class AppLogger : ILogger
    {
        private readonly log4net.ILog _log;
        private readonly IAppSettingFactory _appSettingFactory;

        public AppLogger(IAppSettingFactory appSettingFactory)
        {
            XmlConfigurator.Configure();
            _appSettingFactory = appSettingFactory;
            _log = LogManager.GetLogger(_appSettingFactory.GetSettings().LoggerName);
        }

        public void Log(LogLevel level, string format, params object[] args)
        {
            if (args == null)
            {
                switch (level)
                {
                    case LogLevel.Debug:
                        _log.Debug(format);
                        break;
                    case LogLevel.Information:
                        _log.Info(format);
                        break;
                    case LogLevel.Warning:
                        _log.Warn(format);
                        break;
                    case LogLevel.Error:
                        _log.Error(format);
                        break;
                    case LogLevel.Fatal:
                        _log.Fatal(format);
                        break;
                }
            }
            else
            {
                switch (level)
                {
                    case LogLevel.Debug:
                        _log.DebugFormat(format, args);
                        break;
                    case LogLevel.Information:
                        _log.InfoFormat(format, args);
                        break;
                    case LogLevel.Warning:
                        _log.WarnFormat(format, args);
                        break;
                    case LogLevel.Error:
                        _log.ErrorFormat(format, args);
                        break;
                    case LogLevel.Fatal:
                        _log.FatalFormat(format, args);
                        break;
                }
            }
        }
    }
}