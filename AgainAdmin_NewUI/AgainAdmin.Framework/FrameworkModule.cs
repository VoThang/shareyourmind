﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgainAdmin.Framework.Configuration;
using AgainAdmin.Framework.Logging;
using Autofac;

namespace AgainAdmin.Framework
{
    public class FrameworkModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AppSettingFactory>().As<IAppSettingFactory>().SingleInstance();
            builder.RegisterType<AppSetting>().As<IAppSetting>().SingleInstance();
            builder.RegisterType<AppLogger>().As<ILogger>().SingleInstance();
            //builder.RegisterType<SmtpClient>().As<ISmtpClient>().SingleInstance();
        }
    }
}