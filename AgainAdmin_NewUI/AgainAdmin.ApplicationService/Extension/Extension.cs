﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace AgainAdmin.ApplicationService.Extension
{
    public static class Extension
    {
        public static async Task<IEnumerable<T>> ToListAsynchronous<T>(this IQueryable<T> query)
        {
            return await query.ToListAsync<T>();
        }

        public static async Task<T> FirstOrDefaultAsynchronous<T>(this IQueryable<T> query, Expression<Func<T, bool>> predicate)
        {
            return await query.FirstOrDefaultAsync<T>(predicate);
        }

        public static async Task<bool> AnyAsynchronous<T>(this IQueryable<T> query, Expression<Func<T, bool>> predicate)
        {
            return await query.AnyAsync<T>(predicate);
        }

        public static async Task<IEnumerable<T>> ToListAsynchronous<T>(this IEnumerable<T> query)
        {
            return await query.AsQueryable().ToListAsync<T>();
        }

        public static async Task<T> FirstOrDefaultAsynchronous<T>(this IEnumerable<T> query, Expression<Func<T, bool>> predicate)
        {
            return await query.AsQueryable().FirstOrDefaultAsync<T>(predicate);
        }

        public static async Task<bool> AnyAsynchronous<T>(this IEnumerable<T> query, Expression<Func<T, bool>> predicate)
        {
            return await query.AsQueryable().AnyAsync<T>(predicate);
        }
    }
}