﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AgainAdmin.ApplicationService.Model;
using AgainAdmin.EF.Entity;

namespace AgainAdmin.ApplicationService.Application
{
    public interface IApplicationService
    {
        Task CreateUser(UserModel userModel);
        Task UpdateUser(UserModel userModel);
        Task RemoveUser(UserModel userModel);
        Task<IEnumerable<UserModel>> GetUsers();
        Task<UserModel> GetUserByEmail(string email);
        Task<bool> CreateUserImage(UserImageModel userImage);
        Task<string> GetPasswordByEmail(string email);
        Task<bool> CheckExistEmail(string email);

        Task CreateBlog(BlogModel blogModel);
        Task UpdateBlog(BlogModel blogModel);
        Task RemoveBlog(BlogModel blogModel);
        Task<IEnumerable<BlogModel>> GetBlogs();
        Task<BlogModel> GetBlog(Guid blogId);

        Task CreateComment(CommentModel commentModel);
        Task<IEnumerable<CommentModel>> GetComments(Guid blogId);

        Task<IEnumerable<LikesModel>> GetLikes(Guid blogId);
        Task<IEnumerable<LikesModel>> CreateLike(LikesModel likesModel);
        Task UpdateLike(LikesModel likesModel);

        Task CreateHandleError(HandleError error);
    }
}