﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AgainAdmin.ApplicationService.Model;
using AgainAdmin.ApplicationService.ModelMapper;
using AgainAdmin.EF.Core;
using AgainAdmin.EF.Entity;
using AgainAdmin.EF.Service;
using AgainAdmin.Framework.Logging;

namespace AgainAdmin.ApplicationService.Application.Impl
{
    public class AppService : IApplicationService
    {
        private readonly IUnitOfWorkFactory _uowFactory;
        private readonly IMapperCollector _mapperCollector;
        private readonly IUserService _userService;
        private readonly IHandleErrorService _handleErrorService;
        private readonly IBlogService _blogService;
        private readonly ICommentService _commentService;
        private readonly ILikesService _likesService;
        private readonly IUserImageService _userImageService;
        private readonly IEncryptionService _encryptionService;
        private readonly ILogger _logger;

        public AppService(IMapperCollector mapperCollector, IUnitOfWorkFactory uowFactory, IUserService userService, IHandleErrorService handleErrorService, IBlogService blogService, ICommentService commentService, ILikesService likesService, IUserImageService userImageService, ILogger logger, IEncryptionService encryptionService)
        {
            _mapperCollector = mapperCollector;
            _uowFactory = uowFactory;
            _userService = userService;
            _handleErrorService = handleErrorService;
            _blogService = blogService;
            _commentService = commentService;
            _likesService = likesService;
            _userImageService = userImageService;
            _logger = logger;
            _encryptionService = encryptionService;
        }

        // ------------------------------- User ---------------------------------------- ///
        public async Task<IEnumerable<UserModel>> GetUsers()
        {
            var users = _userService.GetUsers();

            return await MapperUserModelFromEntity(users);
        }

        public async Task<bool> CheckExistEmail(string email)
        {
            try
            {
                var user = await _userService.GetUserByEmail(email);

                return user != null;
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, e.Message);
                throw;
            }
        }

        public async Task CreateUser(UserModel userModel)
        {
            try
            {
                using (var uow = _uowFactory.BeginUnitOfWork())
                {
                    var user = MapperUserFromModel(userModel);
                    user.Password = _encryptionService.EncryptText(userModel.Password);
                    user.Id = Guid.NewGuid();
                    await _userService.CreateUser(user);

                    uow.Commit();
                }
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, e.Message);
                throw;
            }
        }

        public async Task UpdateUser(UserModel userModel)
        {
            try
            {
                using (var uow = _uowFactory.BeginUnitOfWork())
                {
                    var user = MapperUserFromModel(userModel);
                    await _userService.UpdateUser(user);

                    uow.Commit();
                }
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, e.Message);
                throw;
            }
        }

        public async Task RemoveUser(UserModel userModel)
        {
            try
            {
                using (var uow = _uowFactory.BeginUnitOfWork())
                {
                    var user = MapperUserFromModel(userModel);
                    await _userService.RemoveUser(user);

                    uow.Commit();
                }
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, e.Message);
                throw;
            }
        }

        public async Task<UserModel> GetUserByEmail(string email)
        {
            var user = await _userService.GetUserByEmail(email);
            var userModel = MapperUserModelFromEntity(user);
            var userImage = await _userImageService.GetUserImage(user.Id.ToString());
            userModel.UserImage = MapperUserImageModelFromEntity(userImage);

            return userModel;
        }

        public async Task<bool> CreateUserImage(UserImageModel userImageModel)
        {
            var userImage = await _userImageService.GetUserImage(userImageModel.UserId);
            
            if (userImage != null)
            {
                CopyUserImageFromModel(userImageModel, userImage);
                await _userImageService.UpdateUserImage(userImage);
            }
            else
            {
                var newUserImage = MapperUserImageFromModel(userImageModel);
                await _userImageService.CreateUserImage(newUserImage);
            }

            return true;
        }

        public async Task<string> GetPasswordByEmail(string email)
        {
            var user = await _userService.GetUserByEmail(email);

            return user != null ? user.Password : string.Empty;
        }

        // ------------------------------- Handle Error ---------------------------------------- ///
        public async Task CreateHandleError(HandleError error)
        {
            try
            {
                using (var uow = _uowFactory.BeginUnitOfWork())
                {
                    error.Id = Guid.NewGuid();
                    await _handleErrorService.CreateHandleError(error);

                    uow.Commit();
                }
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, e.Message);
                throw;
            }
        }

        // ------------------------------- Blog ---------------------------------------- ///
        public async Task<IEnumerable<BlogModel>> GetBlogs()
        {
            var blogs = _blogService.GetBlogs();
            var blogsModels = await MapperBlogModelFromEntity(blogs);

            return blogsModels;
        }

        public async Task<BlogModel> GetBlog(Guid id)
        {
            try
            {
                var blog = await _blogService.GetBlog(id);

                var userImage = await _userImageService.GetUserImage(blog.UserId);
                var user = await _userService.GetUser(blog.UserId);

                var likes = _likesService.GetLikes(blog.Id);
                var likeModels = await MapperLikesModelFromEntity(likes);
                var likesModels = likeModels as IList<LikesModel> ?? likeModels.ToList();
                foreach (var like in likesModels)
                {
                    var likeUserImage = await _userImageService.GetUserImage(like.UserId);
                    like.UserImage = MapperUserImageModelFromEntity(likeUserImage);
                    var likeUser = await _userService.GetUser(like.UserId);
                    like.User = MapperUserModelFromEntity(likeUser);
                }

                var comments = _commentService.GetComments(blog.Id);
                var commentModel = await MapperCommentModelFromEntity(comments);
                var commentModels = commentModel as IList<CommentModel> ?? commentModel.ToList();
                foreach (var cmt in commentModels)
                {
                    var cmtUserImage = await _userImageService.GetUserImage(cmt.UserId);
                    cmt.UserImage = MapperUserImageModelFromEntity(cmtUserImage);
                    var cmtUser = await _userService.GetUser(cmt.UserId);
                    cmt.User = MapperUserModelFromEntity(cmtUser);
                }

                var blogModel = MapperBlogModelFromEntity(blog);
                blogModel.Likes = likesModels;
                blogModel.Comments = commentModels;
                blogModel.UserImage = MapperUserImageModelFromEntity(userImage);
                blogModel.User = MapperUserModelFromEntity(user);

                return blogModel;
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, e.Message);
                throw;
            }
        }

        public async Task CreateBlog(BlogModel blogModel)
        {
            try
            {
                using (var uow = _uowFactory.BeginUnitOfWork())
                {
                    var blog = MapperBlogFromModel(blogModel);
                    blog.Id = Guid.NewGuid();
                    await _blogService.CreateBlog(blog);

                    uow.Commit();
                }
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, e.Message);
                throw;
            }
        }

        public async Task UpdateBlog(BlogModel blogModel)
        {
            try
            {
                using (var uow = _uowFactory.BeginUnitOfWork())
                {
                    var orgBlog = await _blogService.GetBlog(blogModel.Id);
                    CopyBlogFromModel(blogModel, orgBlog);
                    await _blogService.UpdateBlog(orgBlog);

                    uow.Commit();
                }
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, e.Message);
                throw;
            }
        }

        public async Task RemoveBlog(BlogModel blogModel)
        {
            try
            {
                using (var uow = _uowFactory.BeginUnitOfWork())
                {
                    var blog = MapperBlogFromModel(blogModel);
                    await _blogService.RemoveBlog(blog);

                    uow.Commit();
                }
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, e.Message);
                throw;
            }
        }

        // ------------------------------- Comment ---------------------------------------- ///
        public async Task<IEnumerable<CommentModel>> GetComments(Guid blogId)
        {
            var comments = _commentService.GetComments(blogId);

            return await MapperCommentModelFromEntity(comments);
        }

        public async Task CreateComment(CommentModel commentModel)
        {
            try
            {
                using (var uow = _uowFactory.BeginUnitOfWork())
                {
                    var comment = MapperCommentFromModel(commentModel);
                    comment.Id = Guid.NewGuid();
                    await _commentService.CreateComment(comment);

                    uow.Commit();
                }
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, e.Message);
                throw;
            }
        }

        // ------------------------------- Likes ---------------------------------------- ///
        public async Task<IEnumerable<LikesModel>> GetLikes(Guid blogId)
        {
            var likes = _likesService.GetLikes(blogId);

            return await MapperLikesModelFromEntity(likes);
        }

        public async Task<IEnumerable<LikesModel>> CreateLike(LikesModel likesModel)
        {
            try
            {
                using (var uow = _uowFactory.BeginUnitOfWork())
                {
                    var like = await _likesService.GetLikeByBlogIdAndUserId(likesModel.BlogId, likesModel.UserId);

                    if (like != null)
                    {
                        like.IsLike = !like.IsLike;
                        await _likesService.UpdateLike(like);
                        likesModel.IsLike = like.IsLike;
                    }
                    else
                    {
                        var likeModel = MapperLikesFromModel(likesModel);
                        likeModel.Id = Guid.NewGuid();
                        await _likesService.CreateLike(likeModel);
                        likesModel.IsLike = true;
                    }

                    uow.Commit();
                }

                var likes = _likesService.GetLikes(new Guid(likesModel.BlogId));
                var likesModels = await MapperLikesModelFromEntity(likes);;
                var enumerable = likesModels as IList<LikesModel> ?? likesModels.ToList();
                var likeReturn = new List<LikesModel>();
                foreach (var lik in enumerable)
                {
                    if (!lik.IsLike)
                    {
                        continue;
                    }

                    var likUserImage = await _userImageService.GetUserImage(lik.UserId);
                    lik.UserImage = MapperUserImageModelFromEntity(likUserImage);
                    var likUser = await _userService.GetUser(lik.UserId);
                    lik.User = MapperUserModelFromEntity(likUser);

                    likeReturn.Add(lik);
                }

                return likeReturn;
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, e.Message);

                throw;
            }
        }

        public async Task UpdateLike(LikesModel likesModel)
        {
            try
            {
                using (var uow = _uowFactory.BeginUnitOfWork())
                {
                    var orgLikes = await _likesService.GetLike(likesModel.Id);
                    CopyLikesFromModel(likesModel, orgLikes);
                    await _likesService.UpdateLike(orgLikes);

                    uow.Commit();
                }
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, e.Message);
                throw;
            }
        }

        // ------------------------------- Mapper User ---------------------------------------- ///
        private UserModel MapperUserModelFromEntity(User user)
        {
            return _mapperCollector.UserMapper.ToModel(user);
        }
        private async Task<IEnumerable<UserModel>> MapperUserModelFromEntity(IEnumerable<User> users)
        {
            return await _mapperCollector.UserMapper.ToModel(users);
        }
        private User MapperUserFromModel(UserModel userModel)
        {
            return _mapperCollector.UserMapper.ToEntity(userModel);
        }

        // ------------------------------- Mapper Blog ---------------------------------------- ///
        private async Task<IEnumerable<BlogModel>> MapperBlogModelFromEntity(IEnumerable<Blog> blogs)
        {
            return await _mapperCollector.BlogMapper.ToModel(blogs);
        }
        private BlogModel MapperBlogModelFromEntity(Blog blog)
        {
            return _mapperCollector.BlogMapper.ToModel(blog);
        }
        private Blog MapperBlogFromModel(BlogModel blogModel)
        {
            return _mapperCollector.BlogMapper.ToEntity(blogModel);
        }
        private void CopyBlogFromModel(BlogModel blogModel, Blog blog)
        {
            _mapperCollector.BlogMapper.ToEntity(blogModel, blog);
        }

        // ------------------------------- Mapper Comment ---------------------------------------- ///
        private async Task<IEnumerable<CommentModel>> MapperCommentModelFromEntity(IQueryable<Comment> comments)
        {
            return await _mapperCollector.CommentMapper.ToModel(comments);
        }
        private Comment MapperCommentFromModel(CommentModel commentModel)
        {
            return _mapperCollector.CommentMapper.ToEntity(commentModel);
        }
        private void CopyCommentFromModel(CommentModel commentModel, Comment comment)
        {
            _mapperCollector.CommentMapper.ToEntity(commentModel, comment);
        }

        // ------------------------------- Mapper Likes ---------------------------------------- ///
        private async Task<IEnumerable<LikesModel>> MapperLikesModelFromEntity(IEnumerable<Likes> likes)
        {
            return await _mapperCollector.LikesMapper.ToModel(likes);
        }
        private Likes MapperLikesFromModel(LikesModel likesModel)
        {
            return _mapperCollector.LikesMapper.ToEntity(likesModel);
        }
        private void CopyLikesFromModel(LikesModel likesModel, Likes likes)
        {
            _mapperCollector.LikesMapper.ToEntity(likesModel, likes);
        }

        // ------------------------------- Mapper UserImage ---------------------------------------- ///
        private UserImageModel MapperUserImageModelFromEntity(UserImage userImage)
        {
            return _mapperCollector.UserImageMapper.ToModel(userImage);
        }
        private UserImage MapperUserImageFromModel(UserImageModel userImageModel)
        {
            return _mapperCollector.UserImageMapper.ToEntity(userImageModel);
        }
        private void CopyUserImageFromModel(UserImageModel userImageModel, UserImage userImage)
        {
            _mapperCollector.UserImageMapper.ToEntity(userImageModel, userImage);
        }
    }
}