﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgainAdmin.ApplicationService.Application;
using AgainAdmin.ApplicationService.Application.Impl;
using AgainAdmin.ApplicationService.ModelMapper;
using Autofac;
using Autofac.Extras.AggregateService;

namespace AgainAdmin.ApplicationService
{
    public class ApplicationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAggregateService<IMapperCollector>();
            builder.RegisterType<UserMapper>().SingleInstance();
            builder.RegisterType<BlogMapper>().SingleInstance();
            builder.RegisterType<CommentMapper>().SingleInstance();
            builder.RegisterType<LikesMapper>().SingleInstance();
            builder.RegisterType<UserImageMapper>().SingleInstance();

            builder.RegisterType<AppService>().As<IApplicationService>().InstancePerLifetimeScope();
            builder.RegisterType<EncryptionService>().As<IEncryptionService>().InstancePerLifetimeScope();
        }
    }
}
