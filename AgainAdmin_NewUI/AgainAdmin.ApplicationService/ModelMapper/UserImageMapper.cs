﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AgainAdmin.ApplicationService.Extension;
using AgainAdmin.ApplicationService.Model;
using AgainAdmin.EF.Entity;
using AutoMapper;

namespace AgainAdmin.ApplicationService.ModelMapper
{
    public class UserImageMapper
    {
        static UserImageMapper()
        {
            // ToDataModel
            Mapper.CreateMap<UserImage, UserImageModel>();

            // ToEntity
            Mapper.CreateMap<UserImageModel, UserImage>();
        }

        public UserImageModel ToModel(UserImage userImage)
        {
            return Mapper.Map<UserImage, UserImageModel>(userImage);
        }

        public async Task<IEnumerable<UserImageModel>> ToModel(IEnumerable<UserImage> userImages)
        {
            var userTask = await userImages.ToListAsynchronous<UserImage>();
            return Mapper.Map<IEnumerable<UserImage>, IEnumerable<UserImageModel>>(userTask);
        }

        // generate new entity
        public UserImage ToEntity(UserImageModel userImageModel)
        {
            return Mapper.Map<UserImageModel, UserImage>(userImageModel);
        }

        // copy properties from model to entity
        public void ToEntity(UserImageModel userImageModel, UserImage userImage)
        {
            Mapper.Map<UserImageModel, UserImage>(userImageModel, userImage);
        }

        public IEnumerable<UserImage> ToEntity(IEnumerable<UserImageModel> userImageModels)
        {
            return Mapper.Map<IEnumerable<UserImageModel>, IEnumerable<UserImage>>(userImageModels);
        }
    }
}