﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AgainAdmin.ApplicationService.Extension;
using AgainAdmin.ApplicationService.Model;
using AgainAdmin.EF.Entity;
using AutoMapper;

namespace AgainAdmin.ApplicationService.ModelMapper
{
    public class BlogMapper
    {
        static BlogMapper()
        {
            // ToDataModel
            Mapper.CreateMap<Blog, BlogModel>().ForMember(
                                                dest => dest.CreateDate,
                                                opt => opt.MapFrom(x => x.CreateDate.ToString("dd mmmm yyyy HH:mm")))
                                               .ForMember(
                                                dest => dest.UpdateDate,
                                                opt => opt.MapFrom(x => x.UpdateDate.HasValue ? x.UpdateDate.Value.ToString("dd mmmm yyyy HH:mm") : "N/A"))
                                               .ForMember(
                                                dest => dest.IsUpdate,
                                                opt => opt.MapFrom(x => x.UpdateDate.HasValue ? true : false));

            // ToEntity
            Mapper.CreateMap<BlogModel, Blog>().ForMember(
                                                dest => dest.CreateDate,
                                                opt => opt.MapFrom(x => DateTime.Now))
                                               .ForMember(
                                                dest => dest.UpdateDate,
                                                opt => opt.MapFrom(x => x.IsUpdate ? DateTime.Now : (DateTime?)null));
        }

        public BlogModel ToModel(Blog blog)
        {
            return Mapper.Map<Blog, BlogModel>(blog);
        }

        public async Task<IEnumerable<BlogModel>> ToModel(IEnumerable<Blog> blogs)
        {
            var blogTask = await blogs.ToListAsynchronous<Blog>();
            return Mapper.Map<IEnumerable<Blog>, IEnumerable<BlogModel>>(blogTask);
        }

        // generate new entity
        public Blog ToEntity(BlogModel blogModel)
        {
            return Mapper.Map<BlogModel, Blog>(blogModel);
        }

        // copy properties to entity
        public void ToEntity(BlogModel blogModel, Blog blog)
        {
            Mapper.Map<BlogModel, Blog>(blogModel, blog);
        }

        public IEnumerable<Blog> ToEntity(IEnumerable<BlogModel> blogModel)
        {
            return Mapper.Map<IEnumerable<BlogModel>, IEnumerable<Blog>>(blogModel);
        }
    }
}