﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AgainAdmin.ApplicationService.Extension;
using AgainAdmin.ApplicationService.Model;
using AgainAdmin.EF.Entity;
using AutoMapper;

namespace AgainAdmin.ApplicationService.ModelMapper
{
    public class CommentMapper
    {
        static CommentMapper()
        {
            // ToDataModel
            Mapper.CreateMap<Comment, CommentModel>().ForMember(
                                                dest => dest.CreateDate,
                                                opt => opt.MapFrom(x => x.CreateDate.ToString("yyyy mmmm dd", CultureInfo.CreateSpecificCulture("en-US"))));
                                               
            // ToEntity
            Mapper.CreateMap<CommentModel, Comment>().ForMember(
                                                dest => dest.CreateDate,
                                                opt => opt.MapFrom(x => DateTime.Now));
        }

        public CommentModel ToModel(Comment comment)
        {
            return Mapper.Map<Comment, CommentModel>(comment);
        }

        public async Task<IEnumerable<CommentModel>> ToModel(IEnumerable<Comment> comments)
        {
            var commentTask = await comments.ToListAsynchronous<Comment>();
            return Mapper.Map<IEnumerable<Comment>, IEnumerable<CommentModel>>(commentTask);
        }

        // generate new entity
        public Comment ToEntity(CommentModel commentModel)
        {
            return Mapper.Map<CommentModel, Comment>(commentModel);
        }

        // copy properties to entity
        public void ToEntity(CommentModel commentModel, Comment comment)
        {
            Mapper.Map<CommentModel, Comment>(commentModel, comment);
        }

        public IEnumerable<Comment> ToEntity(IEnumerable<CommentModel> commentModels)
        {
            return Mapper.Map<IEnumerable<CommentModel>, IEnumerable<Comment>>(commentModels);
        }
    }
}