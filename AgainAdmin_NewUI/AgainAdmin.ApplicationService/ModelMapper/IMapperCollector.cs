﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgainAdmin.ApplicationService.ModelMapper
{
    public interface IMapperCollector
    {
        UserMapper UserMapper { get; set; }
        BlogMapper BlogMapper { get; set; }
        CommentMapper CommentMapper { get; set; }
        LikesMapper LikesMapper { get; set; }
        UserImageMapper UserImageMapper { get; set; }
    }
}