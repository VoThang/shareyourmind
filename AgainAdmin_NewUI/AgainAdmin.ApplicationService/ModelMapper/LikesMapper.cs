﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AgainAdmin.ApplicationService.Extension;
using AgainAdmin.ApplicationService.Model;
using AgainAdmin.EF.Entity;
using AutoMapper;

namespace AgainAdmin.ApplicationService.ModelMapper
{
    public class LikesMapper
    {
        static LikesMapper()
        {
            // ToDataModel
            Mapper.CreateMap<Likes, LikesModel>();
                                               
            // ToEntity
            Mapper.CreateMap<LikesModel, Likes>();
        }

        public LikesModel ToModel(Likes likes)
        {
            return Mapper.Map<Likes, LikesModel>(likes);
        }

        public async Task<IEnumerable<LikesModel>> ToModel(IEnumerable<Likes> likes)
        {
            var likesTask = await likes.ToListAsynchronous<Likes>();
            return Mapper.Map<IEnumerable<Likes>, IEnumerable<LikesModel>>(likesTask);
        }

        // generate new entity
        public Likes ToEntity(LikesModel likesModel)
        {
            return Mapper.Map<LikesModel, Likes>(likesModel);
        }

        // copy properties to entity
        public void ToEntity(LikesModel likesModel, Likes likes)
        {
            Mapper.Map<LikesModel, Likes>(likesModel, likes);
        }

        public IEnumerable<Likes> ToEntity(IEnumerable<LikesModel> likesModels)
        {
            return Mapper.Map<IEnumerable<LikesModel>, IEnumerable<Likes>>(likesModels);
        }
    }
}