﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AgainAdmin.ApplicationService.Extension;
using AgainAdmin.ApplicationService.Model;
using AgainAdmin.EF.Entity;
using AutoMapper;

namespace AgainAdmin.ApplicationService.ModelMapper
{
    public class UserMapper
    {
        static UserMapper()
        {
            // ToDataModel
            Mapper.CreateMap<User, UserModel>().ForMember(
                                                dest => dest.FullName,
                                                opt => opt.MapFrom(x => string.Format("{0} {1}", x.FirstName, x.LastName)))
                                                .ForMember(
                                                dest => dest.Password,
                                                opt => opt.MapFrom(x => string.Empty))
                                               .ForMember(
                                                dest => dest.Skills,
                                                opt => opt.MapFrom(x => x.Skills.Split(',')));

            // ToEntity
            Mapper.CreateMap<UserModel, User>().ForMember(
                                                dest => dest.Skills,
                                                opt => opt.MapFrom(x => string.Join(",", x.Skills.Select(i => i))));
        }

        public UserModel ToModel(User user)
        {
            return Mapper.Map<User, UserModel>(user);
        }

        public async Task<IEnumerable<UserModel>> ToModel(IEnumerable<User> users)
        {
            var userTask = await users.ToListAsynchronous<User>();
            return Mapper.Map<IEnumerable<User>, IEnumerable<UserModel>>(userTask);
        }

        // generate new entity
        public User ToEntity(UserModel userModel)
        {
            return Mapper.Map<UserModel, User>(userModel);
        }

        // copy properties to entity
        public void ToEntity(UserModel userModel, User user)
        {
            Mapper.Map<UserModel, User>(userModel, user);
        }

        public IEnumerable<User> ToEntity(IEnumerable<UserModel> userModel)
        {
            return Mapper.Map<IEnumerable<UserModel>, IEnumerable<User>>(userModel);
        }
    }
}