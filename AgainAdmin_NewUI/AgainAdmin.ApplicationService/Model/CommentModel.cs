﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace AgainAdmin.ApplicationService.Model
{
    public class CommentModel : BaseModel
    {
        public string UserId { get; set; }
        public string BlogId { get; set; }
        public string Content { get; set; }
        public string CreateDate { get; set; }

        public UserImageModel UserImage { get; set; }
        public UserModel User { get; set; }
    }
}