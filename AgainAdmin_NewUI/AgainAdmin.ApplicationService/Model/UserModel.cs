﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace AgainAdmin.ApplicationService.Model
{
    public class UserModel : BaseModel
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string Password { get; set; }
        public string Education { get; set; }
        public string Location { get; set; }
        public string[] Skills { get; set; }
        public string About { get; set; }

        public UserImageModel UserImage { get; set; }
    }
}
