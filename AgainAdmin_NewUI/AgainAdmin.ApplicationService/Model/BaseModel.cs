﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgainAdmin.ApplicationService.Model
{
    public abstract class BaseModel
    {
        public virtual Guid Id { get; set; }
    }
}