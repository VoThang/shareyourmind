﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace AgainAdmin.ApplicationService.Model
{
    public class BlogModel : BaseModel
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public string UserId { get; set; }
        public string CreateDate { get; set; }
        public string UpdateDate { get; set; }
        public bool IsUpdate { get; set; }

        public IEnumerable<LikesModel> Likes { get; set; }
        public IEnumerable<CommentModel> Comments { get; set; }
        public UserImageModel UserImage { get; set; }
        public UserModel User { get; set; }
    }
}