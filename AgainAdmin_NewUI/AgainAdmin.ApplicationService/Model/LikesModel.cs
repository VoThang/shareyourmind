﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace AgainAdmin.ApplicationService.Model
{
    public class LikesModel : BaseModel
    {
        public string UserId { get; set; }
        public string BlogId { get; set; }
        public bool IsLike { get; set; }

        public UserImageModel UserImage { get; set; }
        public UserModel User { get; set; }
    }
}