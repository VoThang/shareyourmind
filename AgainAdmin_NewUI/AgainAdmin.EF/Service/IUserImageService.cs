﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgainAdmin.EF.Entity;
using System.Threading.Tasks;

namespace AgainAdmin.EF.Service
{
    public interface IUserImageService
    {
        Task<UserImage> GetUserImage(string userId);
        Task CreateUserImage(UserImage userImage);
        Task UpdateUserImage(UserImage userImage);
    }
}