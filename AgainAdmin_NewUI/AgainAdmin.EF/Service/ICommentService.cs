﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgainAdmin.EF.Entity;
using System.Threading.Tasks;

namespace AgainAdmin.EF.Service
{
    public interface ICommentService
    {
        IQueryable<Comment> GetComments(Guid blogId);
        Task CreateComment(Comment comment);
    }
}