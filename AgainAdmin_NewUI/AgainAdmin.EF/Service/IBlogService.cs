﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgainAdmin.EF.Entity;
using System.Threading.Tasks;

namespace AgainAdmin.EF.Service
{
    public interface IBlogService
    {
        Task<Blog> GetBlog(Guid id);
        IEnumerable<Blog> GetBlogs();
        Task CreateBlog(Blog blog);
        Task UpdateBlog(Blog blog);
        Task RemoveBlog(Blog blog);
    }
}