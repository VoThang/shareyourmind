﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgainAdmin.EF.Entity;
using System.Threading.Tasks;

namespace AgainAdmin.EF.Service
{
    public interface IUserService
    {
        IEnumerable<User> GetUsers();
        Task<User> GetUser(string id);
        Task<User> GetUserByEmail(string email);
        Task<User> GetUserById(Guid id);
        Task CreateUser(User user);
        Task UpdateUser(User user);
        Task RemoveUser(User user);
    }
}