﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgainAdmin.EF.Entity;
using System.Threading.Tasks;

namespace AgainAdmin.EF.Service
{
    public interface ILikesService
    {
        IQueryable<Likes> GetLikes(Guid blogId);
        Task<Likes> GetLike(Guid id);
        Task<Likes> GetLikeByBlogIdAndUserId(string blogId, string userId);
        Task CreateLike(Likes like);
        Task UpdateLike(Likes like);
    }
}