﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AgainAdmin.EF.Entity;
using AgainAdmin.EF.Repository;

namespace AgainAdmin.EF.Service.Impl
{
    public class CommentService : ICommentService
    {
        private readonly ICommentRepository _commentRepository;

        public CommentService(ICommentRepository commentRepository)
        {
            _commentRepository = commentRepository;
        }

        public IQueryable<Comment> GetComments(Guid blogId)
        {
            return _commentRepository.Table.Where(x => x.BlogId.Equals(blogId.ToString()));
        }

        public async Task CreateComment(Comment comment)
        {
            await _commentRepository.Insert(comment);
        }
    }
}