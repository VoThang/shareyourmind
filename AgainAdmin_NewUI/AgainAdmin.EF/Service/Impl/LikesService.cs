﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AgainAdmin.EF.Entity;
using AgainAdmin.EF.Repository;

namespace AgainAdmin.EF.Service.Impl
{
    public class LikesService : ILikesService
    {
        private readonly ILikesRepository _likesRepository;

        public LikesService(ILikesRepository likesRepository)
        {
            _likesRepository = likesRepository;
        }

        public IQueryable<Likes> GetLikes(Guid blogId)
        {
            return _likesRepository.Table.Where(x => x.BlogId.Equals(blogId.ToString()) && x.IsLike);
        }

        public Task<Likes> GetLike(Guid id)
        {
            return _likesRepository.GetById(id);
        }

        public async Task<Likes> GetLikeByBlogIdAndUserId(string blogId, string userId)
        {
            return await _likesRepository.Table.SingleOrDefaultAsync(x => x.BlogId.Equals(blogId) && x.UserId.Equals(userId));
        }

        public async Task CreateLike(Likes likes)
        {
            await _likesRepository.Insert(likes);
        }

        public async Task UpdateLike(Likes likes)
        {
            await _likesRepository.Update(likes);
        }
    }
}