﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AgainAdmin.EF.Entity;
using AgainAdmin.EF.Repository;

namespace AgainAdmin.EF.Service.Impl
{
    public class HandleErrorService : IHandleErrorService
    {
        private readonly IHandleErrorRepository _handleErrorRepository;

        public HandleErrorService(IHandleErrorRepository handleErrorRepository)
        {
            _handleErrorRepository = handleErrorRepository;
        }

        public async Task CreateHandleError(HandleError error)
        {
            await _handleErrorRepository.Insert(error);
        }
    }
}