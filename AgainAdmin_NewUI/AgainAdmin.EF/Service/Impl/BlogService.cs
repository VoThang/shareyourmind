﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AgainAdmin.EF.Entity;
using AgainAdmin.EF.Repository;

namespace AgainAdmin.EF.Service.Impl
{
    public class BlogService : IBlogService
    {
        private readonly IBlogRepository _blogRepository;

        public BlogService(IBlogRepository blogRepository)
        {
            _blogRepository = blogRepository;
        }

        public Task<Blog> GetBlog(Guid id)
        {
            return _blogRepository.GetById(id);
        }

        public IEnumerable<Blog> GetBlogs()
        {
            return _blogRepository.Table;
        }

        public async Task CreateBlog(Blog blog)
        {
            await _blogRepository.Insert(blog);
        }

        public async Task UpdateBlog(Blog blog)
        {
            await _blogRepository.Update(blog);
        }

        public async Task RemoveBlog(Blog blog)
        {
            await _blogRepository.Delete(blog);
        }
    }
}