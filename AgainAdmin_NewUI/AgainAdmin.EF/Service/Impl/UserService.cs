﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AgainAdmin.EF.Entity;
using AgainAdmin.EF.Repository;

namespace AgainAdmin.EF.Service.Impl
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public IEnumerable<User> GetUsers()
        {
            return _userRepository.Table;
        }

        public async Task<User> GetUser(string id)
        {
            return await _userRepository.Table.SingleOrDefaultAsync(x => x.Id.ToString().Equals(id));
        }

        public async Task<User> GetUserById(Guid id)
        {
            return await _userRepository.GetById(id);
        }

        public async Task<User> GetUserByEmail(string email)
        {
            return await _userRepository.Table.SingleOrDefaultAsync(x => x.Email.Equals(email));
        }

        public async Task CreateUser(User user)
        {
            await _userRepository.Insert(user);
        }

        public async Task UpdateUser(User user)
        {
            await _userRepository.Update(user);
        }

        public async Task RemoveUser(User user)
        {
            await _userRepository.Delete(user);
        }
    }
}