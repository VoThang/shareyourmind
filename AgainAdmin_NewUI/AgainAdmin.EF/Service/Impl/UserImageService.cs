﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AgainAdmin.EF.Entity;
using AgainAdmin.EF.Repository;

namespace AgainAdmin.EF.Service.Impl
{
    public class UserImageService : IUserImageService
    {
        private readonly IUserImageRepository _userImageRepository;

        public UserImageService(IUserImageRepository userImageRepository)
        {
            _userImageRepository = userImageRepository;
        }

        public async Task<UserImage> GetUserImage(string userId)
        {
            return await _userImageRepository.Table.SingleOrDefaultAsync(x => x.UserId.Equals(userId));
        }

        public async Task CreateUserImage(UserImage userImage)
        {
            await _userImageRepository.Insert(userImage);
        }

        public async Task UpdateUserImage(UserImage userImage)
        {
            await _userImageRepository.Update(userImage);
        }
    }
}