﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AgainAdmin.EF.Entity;

namespace AgainAdmin.EF.Repository
{
    public partial interface IRepository<T>
    {
        Task<T> GetById(object id);
        Task Insert(T entity);
        Task<Guid> InsertAndGetId(T entity);
        Task Update(T entity);
        Task Delete(T entity);
        IQueryable<T> Table { get; }
    }
}