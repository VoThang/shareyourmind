﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using AgainAdmin.EF.Entity;

namespace AgainAdmin.EF.Repository
{
    public partial class BaseRepository<D, T> : IRepository<T>
        where D : DbContext
        where T : BaseEntity
    {
        protected readonly D _context;
        private DbSet<T> _entities;

        public BaseRepository(D context)
        {
            this._context = context;
        }

        public async Task<T> GetById(object id)
        {
            return await this.Entities.FindAsync(id);
        }

        public async Task Insert(T entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");

                this.Entities.Add(entity);

                await this._context.SaveChangesAsync();
            }
            catch (DbEntityValidationException dbEx)
            {
                throw dbEx;
            }
        }

        public async Task<Guid> InsertAndGetId(T entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");

                this.Entities.Add(entity);

                await this._context.SaveChangesAsync();

                return entity.Id;
            }
            catch (DbEntityValidationException dbEx)
            {
                throw dbEx;
            }
        }

        public async Task Update(T entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");

                await this._context.SaveChangesAsync();
            }
            catch (DbEntityValidationException dbEx)
            {
                throw dbEx;
            }
        }

        public async Task Delete(T entity)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");

                this.Entities.Remove(entity);

                await this._context.SaveChangesAsync();
            }
            catch (DbEntityValidationException dbEx)
            {
                throw dbEx;
            }
        }

        public virtual IQueryable<T> Table
        {
            get { return this.Entities; }
        }

        private DbSet<T> Entities
        {
            get { return _entities ?? (_entities = _context.Set<T>()); }
        }
    }
}