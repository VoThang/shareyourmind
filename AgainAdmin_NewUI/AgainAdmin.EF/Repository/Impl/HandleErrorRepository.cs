﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgainAdmin.EF.Core;
using AgainAdmin.EF.Entity;

namespace AgainAdmin.EF.Repository.Impl
{
    public class HandleErrorRepository : BaseRepository<AppDbContext, HandleError>, IHandleErrorRepository
    {
        public HandleErrorRepository(AppDbContext context)
            : base(context)
        {
        }
    }
}