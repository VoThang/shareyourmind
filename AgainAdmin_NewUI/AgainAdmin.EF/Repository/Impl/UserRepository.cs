﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgainAdmin.EF.Core;
using AgainAdmin.EF.Entity;

namespace AgainAdmin.EF.Repository.Impl
{
    public class UserRepository : BaseRepository<AppDbContext, User>, IUserRepository
    {
        public UserRepository(AppDbContext context) : base(context)
        {
        }
    }
}