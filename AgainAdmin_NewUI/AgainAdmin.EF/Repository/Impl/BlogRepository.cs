﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgainAdmin.EF.Core;
using AgainAdmin.EF.Entity;

namespace AgainAdmin.EF.Repository.Impl
{
    public class BlogRepository : BaseRepository<AppDbContext, Blog>, IBlogRepository
    {
        public BlogRepository(AppDbContext context) : base(context)
        {
        }
    }
}