﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgainAdmin.EF.Core;
using AgainAdmin.EF.Entity;

namespace AgainAdmin.EF.Repository.Impl
{
    public class CommentRepository : BaseRepository<AppDbContext, Comment>, ICommentRepository
    {
        public CommentRepository(AppDbContext context)
            : base(context)
        {
        }
    }
}