﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AgainAdmin.EF.Entity;

namespace AgainAdmin.EF.Repository
{
    public interface IHandleErrorRepository : IRepository<HandleError>
    {
    }
}