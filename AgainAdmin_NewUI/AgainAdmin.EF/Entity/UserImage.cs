﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgainAdmin.EF.Entity
{
    public class UserImage : BaseEntity
    {
        public virtual string UserId { get; set; }
        public virtual string FileNameInS3 { get; set; }
        public virtual string Url { get; set; }
    }
}