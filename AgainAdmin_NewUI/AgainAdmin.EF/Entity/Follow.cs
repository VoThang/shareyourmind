﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgainAdmin.EF.Entity
{
    public class Follow : BaseEntity
    {
        public virtual string OriginalId { get; set; }
        public virtual string DestinationId { get; set; }
    }
}