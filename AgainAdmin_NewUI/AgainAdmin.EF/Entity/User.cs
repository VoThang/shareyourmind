﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgainAdmin.EF.Entity
{
    public class User : BaseEntity
    {
        public virtual string Email { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string Password { get; set; }
        public virtual string Education { get; set; }
        public virtual string Location { get; set; }
        public virtual string Skills { get; set; }
        public virtual string About { get; set; }
    }
}