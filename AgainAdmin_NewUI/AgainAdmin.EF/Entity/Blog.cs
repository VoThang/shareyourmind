﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgainAdmin.EF.Entity
{
    public class Blog : BaseEntity
    {
        public virtual string Title { get; set; }
        public virtual string Content { get; set; }
        public virtual string UserId { get; set; }
        public virtual DateTime CreateDate { get; set; }
        public virtual DateTime? UpdateDate { get; set; }
    }
}