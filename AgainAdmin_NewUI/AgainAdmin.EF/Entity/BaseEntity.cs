﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AgainAdmin.EF.Entity
{
    public partial class BaseEntity
    {
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)] comment here to use Postgree
        public virtual Guid Id { get; set; }
    }
}