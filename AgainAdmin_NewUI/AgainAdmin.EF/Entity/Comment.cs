﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgainAdmin.EF.Entity
{
    public class Comment : BaseEntity
    {
        public virtual string UserId { get; set; }
        public virtual string BlogId { get; set; }
        public virtual string Content { get; set; }
        public virtual DateTime CreateDate { get; set; }
    }
}