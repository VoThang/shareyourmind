﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgainAdmin.EF.Entity
{
    public class HandleError : BaseEntity
    {
        public virtual string ClassName { get; set; }
        public virtual string Method { get; set; }
        public virtual string Message { get; set; }
        public virtual string StackTrace { get; set; }
        public virtual DateTime CreateTime { get; set; }
    }
}