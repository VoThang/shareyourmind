﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using AgainAdmin.EF.Core;
using AgainAdmin.EF.Core.Impl;
using AgainAdmin.EF.Repository;
using AgainAdmin.EF.Repository.Impl;
using AgainAdmin.EF.Service;
using AgainAdmin.EF.Service.Impl;
using Autofac;

namespace AgainAdmin.EF
{
    public class EFModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // Support
            builder.RegisterType<AppDbContext>().InstancePerLifetimeScope();
            builder.Register(x => new TransactionScope(TransactionScopeAsyncFlowOption.Enabled)).InstancePerDependency();
            builder.RegisterType<AgainAdminTransaction>().As<ITransaction>().InstancePerDependency();
            builder.RegisterType<AgainAdminUnitOfWork>().As<IUnitOfWork>().InstancePerDependency();
            builder.RegisterType<AgainAdminUnitOfWorkFactory>().As<IUnitOfWorkFactory>().SingleInstance();

            // Service
            builder.RegisterType<UserService>().As<IUserService>().InstancePerLifetimeScope();
            builder.RegisterType<BlogService>().As<IBlogService>().InstancePerLifetimeScope();
            builder.RegisterType<CommentService>().As<ICommentService>().InstancePerLifetimeScope();
            builder.RegisterType<LikesService>().As<ILikesService>().InstancePerLifetimeScope();
            builder.RegisterType<UserImageService>().As<IUserImageService>().InstancePerLifetimeScope();
            builder.RegisterType<HandleErrorService>().As<IHandleErrorService>().InstancePerLifetimeScope();

            // Repository
            builder.RegisterType<UserRepository>().As<IUserRepository>().InstancePerLifetimeScope();
            builder.RegisterType<BlogRepository>().As<IBlogRepository>().InstancePerLifetimeScope();
            builder.RegisterType<CommentRepository>().As<ICommentRepository>().InstancePerLifetimeScope();
            builder.RegisterType<LikesRepository>().As<ILikesRepository>().InstancePerLifetimeScope();
            builder.RegisterType<UserImageRepository>().As<IUserImageRepository>().InstancePerLifetimeScope();
            builder.RegisterType<HandleErrorRepository>().As<IHandleErrorRepository>().InstancePerLifetimeScope();
        }
    }
}