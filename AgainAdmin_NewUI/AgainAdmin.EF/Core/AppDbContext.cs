﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using AgainAdmin.EF.Entity;

namespace AgainAdmin.EF.Core
{
    public class AppDbContext : DbContext
    {
        public AppDbContext()
            : base("AppDbContext")
        {
            Database.SetInitializer<AppDbContext>(null);
        }

        public IDbSet<User> User { get; set; }
        public IDbSet<UserImage> UserImage { get; set; }
        public IDbSet<Blog> Blog { get; set; }
        public IDbSet<Comment> Comment { get; set; }
        public IDbSet<Likes> Likes { get; set; }
        public IDbSet<HandleError> HandleError { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            try
            {
                modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
                modelBuilder.HasDefaultSchema("public");
                base.OnModelCreating(modelBuilder);
            }
            catch (Exception e)
            {
                throw e;
            }
            
        }
    }
}