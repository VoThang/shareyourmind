﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgainAdmin.EF.Core
{
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork BeginUnitOfWork();
    }
}