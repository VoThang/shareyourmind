﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;

namespace AgainAdmin.EF.Core.Impl
{
    public class AgainAdminTransaction : ITransaction
    {
        private TransactionScope _internalTransaction;

        public AgainAdminTransaction(TransactionScope transactionScope)
        {
            _internalTransaction = transactionScope;
        }

        public void Commit()
        {
            _internalTransaction.Complete();
        }

        public void Rollback()
        {
            // Ultilize USING statement to do the job
        }

        public virtual void Dispose()
        {
            if (_internalTransaction != null)
            {
                _internalTransaction.Dispose();
                _internalTransaction = null;
            }
        }
    }
}