﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Autofac;

namespace AgainAdmin.EF.Core.Impl
{
    public class AgainAdminUnitOfWorkFactory : IUnitOfWorkFactory
    {
        private readonly ILifetimeScope _lifetimeScope;

        public AgainAdminUnitOfWorkFactory(ILifetimeScope lifetimeScope)
        {
            _lifetimeScope = lifetimeScope;
        }

        public IUnitOfWork BeginUnitOfWork()
        {
            return _lifetimeScope.Resolve<IUnitOfWork>();
        }
    }
}