﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace AgainAdmin.EF.Core.Impl
{
    public class AgainAdminUnitOfWork : IUnitOfWork
    {
        private ITransaction _transaction;

        public AgainAdminUnitOfWork(ITransaction transaction)
        {
            _transaction = transaction;
        }

        public void Commit()
        {
            Debug.Assert(_transaction != null);
            _transaction.Commit();
        }

        public virtual void Dispose()
        {
            if (_transaction != null)
            {
                _transaction.Dispose();
                _transaction = null;
            }
        }
    }
}