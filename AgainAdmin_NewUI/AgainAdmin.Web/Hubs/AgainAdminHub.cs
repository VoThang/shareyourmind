﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using AgainAdmin.Web.Utils;
using Autofac;

namespace AgainAdmin.Web.Hubs
{
    public class AgainAdminHub : Hub, IAgainAdminHub
    {
        private readonly AgainAdminManageConnection _manageConnection;
        private readonly ILifetimeScope _lifetimeScope;
        private readonly Common _common;

        public AgainAdminHub(AgainAdminManageConnection manageConnection, ILifetimeScope lifetimeScope, Common common)
        {
            _manageConnection = manageConnection;
            _lifetimeScope = lifetimeScope;
            _common = common;
        }

        public void NotifyNewComment(string connectionId, string commentCnt)
        {
            var context = GlobalHost.ConnectionManager.GetHubContext<AgainAdminHub>();
            context.Clients.Client(connectionId).NotifyNewComment(commentCnt);
        }

        public void NotifyStatusUser(string username, string status)
        {
            var context = GlobalHost.ConnectionManager.GetHubContext<AgainAdminHub>();
            context.Clients.All.NotifyStatusUser(username, status);
        }

        public void SendMsg(string receiver, string msg)
        {
            var sender = Context.User.Identity.Name;
            var now = _common.GetHourMinutesNow();

            // get connection id destination
            var connectionId = _manageConnection.GetConnectonId(receiver);

            var context = GlobalHost.ConnectionManager.GetHubContext<AgainAdminHub>();
            context.Clients.Client(connectionId).ReceiveMsg(now, msg, sender);
        }

        [Authorize]
        public override async Task OnConnected()
        {
            var userName = Context.User.Identity.Name;
            var connectionId = Context.ConnectionId;

            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(connectionId))
                return;

            _manageConnection.AddConnection(userName, connectionId);
            NotifyStatusUser(userName, "online");
        }

        [Authorize]
        public override async Task OnReconnected()
        {
            var userName = Context.User.Identity.Name;
            var connectionId = Context.ConnectionId;

            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(connectionId))
                return;

            _manageConnection.AddConnection(userName, connectionId);
            NotifyStatusUser(userName, "online");
        }

        [Authorize]
        public override Task OnDisconnected(bool stopCalled)
        {
            if (Context.User == null)
            {
                return base.OnDisconnected(stopCalled);
            }

            var userName = Context.User.Identity.Name;
            var connectionId = Context.ConnectionId;
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(connectionId))
            {
                return base.OnDisconnected(stopCalled);
            }

            _manageConnection.RemoveConnection(userName);
            NotifyStatusUser(userName, "offline");

            return base.OnDisconnected(stopCalled);
        }
    }
}