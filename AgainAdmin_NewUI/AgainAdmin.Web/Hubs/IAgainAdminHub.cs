﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace AgainAdmin.Web.Hubs
{
    public interface IAgainAdminHub
    {
        Task OnConnected();
        Task OnReconnected();
        Task OnDisconnected(bool dis);

        void NotifyNewComment(string connectionId, string commentCnt);
    }
}