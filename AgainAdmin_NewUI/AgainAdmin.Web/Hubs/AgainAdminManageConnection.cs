﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgainAdmin.Web.Hubs
{
    public class AgainAdminManageConnection
    {
        private readonly object _padlock = new object();
        private IDictionary<string, string> _userConnectionDic;

        public IDictionary<string, string> UserConnection
        {
            get
            {
                if (_userConnectionDic == null) CreateUserConnectionDic();
                return _userConnectionDic;
            }
        }

        public void CreateUserConnectionDic()
        {
            if (_userConnectionDic != null) return;
            lock (_padlock)
            {
                _userConnectionDic = new Dictionary<string, string>();
            }
        }

        public void AddConnection(string username, string connectionId)
        {
            lock (_padlock)
            {
                if (_userConnectionDic == null)
                {
                    CreateUserConnectionDic();
                }

                if (_userConnectionDic == null) return;
                if (_userConnectionDic.Keys.Contains(username))
                {
                    _userConnectionDic[username] = connectionId;
                }
                else
                {
                    _userConnectionDic.Add(username, connectionId);
                }
            }
        }

        public void RemoveConnection(string username)
        {
            lock (_padlock)
            {
                if (_userConnectionDic == null) return;
                if (_userConnectionDic.ContainsKey(username))
                {
                    _userConnectionDic.Remove(username);
                }
            }
        }

        public string GetConnectonId(string username)
        {
            if (_userConnectionDic == null) return string.Empty;

            if (username == null) return string.Empty;

            return (_userConnectionDic.ContainsKey(username)) ? _userConnectionDic[username] : string.Empty;
        }

        public void Flush()
        {
            _userConnectionDic = null;
        }
    }
}