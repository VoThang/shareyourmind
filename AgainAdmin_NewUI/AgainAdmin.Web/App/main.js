﻿// Maps the files so Durandal knows where to find these.
require.config({
    paths: {
        'text': '../Scripts/text',
        'durandal': '../Scripts/durandal',
        'plugins': '../Scripts/durandal/plugins',
        'transitions': '../Scripts/durandal/transitions'
    },

});

// Durandal 2.x assumes no global libraries. It will ship expecting 
// Knockout and jQuery to be defined with requirejs. .NET 
// templates by default will set them up as standard script
// libs and then register them with require as follows: 
define('jquery', function () { return jQuery; });
define('knockout', ko);
define(['durandal/app', 'durandal/viewLocator', 'durandal/system', 'plugins/router', 'services/logger', 'services/auth'], boot);

function boot(app, viewLocator, system, router, logger, auth) {

    // Enable debug message to show in the console 
    //system.debug(true);

    app.title = 'Share Minds';

    app.configurePlugins({
        router: true,
        dialog: true 
    });

    app.start().then(function () {
        // When finding a viewmodel module, replace the viewmodel string 
        // with view to find it partner view.
        // [viewmodel]s/sessions --> [view]s/sessions.html
        // Defaults to viewmodels/views/views. 
        // Otherwise you can pass paths for modules, views, partials
        viewLocator.useConvention();

        //app.setRoot('viewmodels/shell');
        var token = $.cookie('token');
        if (token != null && token.length > 0) {
            var email = $.cookie('email');
            auth.getUserInformation(email);
            app.setRoot('viewmodels/shell');
        } else {
            app.setRoot('viewmodels/login');
        }
    });
};