﻿define(['jquery', 'knockout', 'transitions/entrance', 'plugins/dialog', 'bootstrap'],
    // Create a dialog using Bootstrap 3
    function ($, ko, entrance, dialog) {
        return {
            addHost: function (theDialog) {
                var body = $('body');
                $('<div class="modal fade" id="myModal"></div>').appendTo(body);
                theDialog.host = $('#myModal').get(0);
            },
            removeHost: function (theDialog) {
                setTimeout(function () {
                    $('#myModal').modal('hide');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                }, 200);

            },
            compositionComplete: function (child, parent, context) {
                var theDialog = dialog.getDialog(context.model);
                $('#myModal').modal('show');
            }
        };
    });