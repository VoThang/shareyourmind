﻿define(['plugins/router'],
    function (router) {

        var gotoDashBoard = function() {
            router.navigate("#dashboard");
        };

        var gotoAllMinds = function() {
            router.navigate("#allblogs");
        };

        var gotoNewMind = function() {
            router.navigate("#newblog");
        };

        var gotoGallery = function() {
            router.navigate("#gallery");
        };

        return {
            gotoDashBoard: gotoDashBoard,
            gotoAllMinds: gotoAllMinds,
            gotoNewMind: gotoNewMind,
            gotoGallery: gotoGallery
        }
    });