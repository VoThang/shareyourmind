﻿define(['knockout', 'jquery', 'services/blog',
        'viewmodels/header', 'services/common'],
function (ko, $, srvBlog,
          header, common) {

    var title = ko.observable();
    var content = ko.observable();
    var isUpdate = ko.observable();

    var isProcessing = ko.observable();

    var createBlogRequest = function () {
        return {
            title: title(),
            content: CKEDITOR.instances['newblog'].getData(),
            userId: header.user().id(),
            isUpdate: isUpdate()
        }
    };

    var createBlog = function () {
        var blog = createBlogRequest();

        isProcessing(true);
        srvBlog.createBlog(blog).done(function(result) {
            isProcessing(false);
            if (result.status === "Success") {
                common.notification.succeess("Post new blog sucessfully");
            }
        });
    };

    isProcessing.subscribe(function () {
        var element = $('.statePost');

        if (isProcessing()) {
            element.button('loading');
        } else {
            element.button('reset');
        }
    });

    var attached = function() {
        CKEDITOR.replace('newblog');
    };

    return {
        title: title,
        content: content,

        isProcessing: isProcessing,

        createBlog: createBlog,

        attached: attached
    }
});