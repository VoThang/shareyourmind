﻿define(['knockout', 'jquery', 'services/blog',
    'durandal/app', 'plugins/router', 'viewmodels/blog/detail'],
function (ko, $, srvBlog,
        app, router) {

    var blogs = ko.observableArray([]);

    var blogItem = function() {
        var init = function(data) {
            var id = ko.observable(data.id);
            var title = ko.observable(data.title);

            return {
                id: id,
                title: title
            }
        }

        return {
            init: init
        }
    }();

    var getBlogs = function () {
        blogs([]);

        srvBlog.getBlogs().done(function(result) {
            $.each(result, function(i, item) {
                blogs.push(blogItem.init(item));
            });
        });
    };

    var gotoDetail = function (item) {
        app.trigger("blogDetail", item);
        router.navigate("detailblog");
    };

    var activate = function() {
        getBlogs();
    };

    return {
        blogs: blogs,

        gotoDetail: gotoDetail,

        activate: activate
    }
});