﻿define(['knockout', 'jquery', 'durandal/app',
        'services/blog', 'viewmodels/header', 'services/comment',
        'services/likes'],
function (ko, $, app,
        srvBlog, header, srvComment,
        srvLikes) {

    var id = ko.observable();
    var title = ko.observable();
    var content = ko.observable();
    var createDate = ko.observable();
    var updateDate = ko.observable();
    var isUpdate = ko.observable();
    var user = ko.observable();
    var userImage = ko.observable();
    var likeText = ko.observable();
    var likeDisplay = ko.observable();
    var likes = ko.observableArray([]);
    var comments = ko.observableArray([]);

    var isLiking = ko.observable();
    var isCommenting = ko.observable();

    var userItem = function() {
        var init = function (data) {
            var id = ko.observable();
            var firstName = ko.observable();
            var lastName = ko.observable();
            var fullName = ko.observable();
            var status = ko.observable();

            if (data == null) {
                id("");
                firstName("");
                lastName("");
                fullName("");
                status("");
            } else {
                id(data.id);
                firstName(data.firstName);
                lastName(data.lastName);
                fullName(data.fullName);
                status(data.status);
            }

            return {
                id: id,
                firstName: firstName,
                lastName: lastName,
                fullName: fullName,
                status: status
            }
        }

        return {
            init: init
        }
    }();

    var userImageItem = function() {
        var init = function (data) {
            var url = ko.observable();
            if (data == null) {
                url("../../Content/againadmin/img/user2-160x160.jpg");
            } else {
                url(data.url);
            }

            return {
                url: url
            }
        }

        return {
            init: init
        }
    }();

    var likeItem = function() {
        var init = function(data) {
            var userImage = ko.observable(userImageItem.init(data.userImage));
            var user = ko.observable(userItem.init(data.user));

            return {
                userImage: userImage,
                user: user
            }
        }

        return {
            init: init
        }
    }();

    var commentItem = function() {
        var init = function(data) {
            var content = ko.observable(data.content);
            var createDate = ko.observable(data.createDate);
            var userImage = ko.observable(userImageItem.init(data.userImage));
            var user = ko.observable(userItem.init(data.user));
            
            return {
                content: content,
                createDate: createDate,
                userImage: userImage,
                user: user
            }
        }

        return {
            init: init
        }
    }();

    var setLikesDisplay = function (result) {
        var val = "";

        if (result.length > 0) {
            val = " " + result.length + " likes";
        }

        likeDisplay(val);
    };

    var isLiked = function (result) {
        var userId = header.user().id();
        var userExist = ko.utils.arrayFirst(result, function (item) {
            return item.user.id === userId && item.blogId === id();
        });

        if (userExist) {
            likeText("Liked");
        } else {
            likeText("Like");
        }
    };

    var setLikes = function (data) {
        likes([]);

        $.each(data, function(index, item) {
            likes.push(likeItem.init(item));
        });

        setLikesDisplay(data);
        isLiked(data);
    };

    var setComments = function (data) {
        comments([]);

        $.each(data, function(index, item) {
            comments.push(commentItem.init(item));
        });
    };

    var setBlog = function(data) {
        id(data.id);
        title(data.title);
        content(data.content);
        createDate(data.createDate);
        updateDate(data.updateDate);
        isUpdate(data.isUpdate);

        userImage(userImageItem.init(data.userImage));
        user(userItem.init(data.user));

        setLikes(data.likes);
        setComments(data.comments);
    };

    var commentsDisplay = ko.computed(function() {
        var val = "Comments";

        if (comments().length > 0) {
            val = "Comments" + " (" + comments().length + ")";
        }

        return val;
    });

    var dateDisplay = ko.computed(function() {
        if (updateDate() !== "N/A") {
            return updateDate();
        }

        return createDate();
    });

    var likeBlog = function () {
        if (isLiking()) {
            return;
        }

        var like = {
            blogId: id(),
            userId: header.user().id()
        }
        isLiking(true);
        srvLikes.like(like).done(function (result) {
            isLiking(false);

            setLikesDisplay(result);
            isLiked(result);
        });
    };

    ko.bindingHandlers.commentEnter = {
        init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
            // This will be called when the binding is first applied to an element
            // Set up any initial state, event handlers, etc. here

            $(element).keyup(function (event) {
                if (isCommenting()) {
                    return;
                }

                var code = event.which;

                if (code !== 13) {
                    return;
                }

                isCommenting(true);
                var comment = {
                    userId: header.user().id(),
                    blogId: id(),
                    content: $(element).val()
                }
                srvComment.comment(comment).done(function () {
                    isCommenting(false);
                    $(element).val("");
                    // update comment list

                    var blog = {
                        id: id()
                    };

                    srvComment.getComments(blog).done(function (result) {
                        setComments(result);
                    });
                });
            });
        }
    };

    app.on("blogDetail", function(item) {
        var blog = {
            id: item.id()
        }

        srvBlog.getBlog(blog).done(function(result) {
            setBlog(result);
        });
    });

    var activate = function() {
        user(userItem.init(null));
        userImage(userImageItem.init(null));
    };

    var deactivate = function() {
        id("");
        title("");
        content("");
        updateDate("");
        isUpdate(false);
    };

    return {
        id: id,
        title: title,
        content: content,
        createDate: createDate,
        updateDate: updateDate,
        isUpdate: isUpdate,
        dateDisplay: dateDisplay,
        likeText: likeText,
        isLiking: isLiking,

        likes: likes,
        comments: comments,

        commentsDisplay: commentsDisplay,
        likeDisplay: likeDisplay,

        user: user,
        userImage: userImage,

        likeBlog: likeBlog,

        activate: activate,
        deactivate: deactivate
    }
});