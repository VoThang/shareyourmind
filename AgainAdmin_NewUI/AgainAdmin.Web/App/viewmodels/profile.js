﻿define(['durandal/app', 'plugins/router', 'knockout',
    'jquery', 'services/common', 'services/profile',
    'plugins/dialog', 'viewmodels/gallery/upload'],
function (app, router, ko,
          $, srvCommon, srvProfile,
          dialog, upload) {

    var id = ko.observable();
    var firstName = ko.observable();
    var lastName = ko.observable();
    var fullName = ko.observable();
    var email = ko.observable();
    var status = ko.observable();
    var url = ko.observable();

    var isEditingEducation = ko.observable(false);
    var isEditingLocation = ko.observable(false);
    var isEditingSkills = ko.observable(false);
    var isEditingAbout = ko.observable(false);

    var education = ko.observable();
    var location = ko.observable();
    var skills = ko.observableArray();
    var skillEdit = ko.observable();
    var about = ko.observable();

    var imageUpload = null;
    var uploadDfd = new $.Deferred();

    var isProcessing = ko.observable(false);

    var init = function(data) {
        if (data != null) {
            id(data.id);
            firstName(data.firstName);
            lastName(data.lastName);
            fullName(data.fullName);
            email(data.email);
            status(data.status);
            education(data.education);
            location(data.location);
                
            if (data.skills != null) {
                $.each(data.skills, function(item, index) {
                    skills.push(item);
                });
            }

            skillEdit(skills().join(", "));
            about(data.about);

            if (data.userImage != null) {
                url(data.userImage.url);
            } else {
                url("../../Content/againadmin/img/user2-160x160.jpg");
            }

        }
    };

    function handleFileSelect(evt) {
        evt.stopPropagation();
        evt.preventDefault();

        var files = evt.dataTransfer.files; // FileList object.

        
        // Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {

            // Only process image files.
            if (!f.type.match('image.*')) {
                continue;
            }

            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    // Render thumbnail.
                    var src = e.target.result;
                    url(src);
                };
            })(f);

            // Read in the image file as a data URL.
            reader.readAsDataURL(f);
        }
    }

    function handleDragOver(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
    }

    var attached = function() {
        // Setup the dnd listeners.
        var dropZone = document.getElementById('drop_zone');
        dropZone.addEventListener('dragover', handleDragOver, false);
        dropZone.addEventListener('drop', handleFileSelect, false);
    };

    var initUpload = function () {
        $("#avatarFile").fileupload({
            url: "/AgainAdmin/UploadProfile",
            type: "POST",
            dataType: 'json',
            add: function (e, data) {
                imageUpload = data;
                uploadDfd = new $.Deferred();
            },
            done: function (e, data) {
                var urlS3 = data.urlS3;

                if (urlS3) {
                    uploadDfd.resolve();
                } else {
                    srvCommon.notification.error("Upload profile image failed");
                }
            },
            error: function () {
            },
            fail: function (e, data) {
                srvCommon.notification.error("Upload profile image failed");
            }
        });
    }();

    var createProfileRequest = function () {
        return {
            id: id(),
            email: email(),
            firstName: firstName(),
            lastName: lastName(),
            fullName: fullName(),
            status: status(),
            education: education(),
            location: location(),
            skills: skills(),
            about: about()
        }
    };

    var updateProfileNotUpload = function () {
        var requestProfile = createProfileRequest();

        isProcessing(true);
        srvProfile.update(requestProfile).done(function (result) {
            isProcessing(false);
            if (result.error === 1) {
                srvCommon.notification.error(result.mes);
                return;
            }

            srvCommon.notification.succeess("Update profile successfully");
        });
    };

    var updateProfile = function() {
        $.when(uploadDfd).done(function () {
            var requestProfile = createProfileRequest();

            isProcessing(true);
            srvProfile.update(requestProfile).done(function (result) {
                isProcessing(false);
                if (result.error === 1) {
                    srvCommon.notification.error(result.mes);
                    return;
                }

                srvCommon.notification.succeess("Update profile successfully");
            });
        });
    };

    var update = function() {
        if (imageUpload != null) {
            imageUpload.submit();
            imageUpload = null;

            updateProfile();
        } else {
            updateProfileNotUpload();
        }
    };

    var activate = function() {
        srvCommon.initDialogBootstrap(dialog);
    };

    var openDialogTest = function () {
        dialog.showBootstrap(upload);
    };

    return {
        fullName: fullName,
        url: url,
        education: education,
        location: location,
        skills: skills,
        skillEdit: skillEdit,
        about: about,

        isEditingEducation: isEditingEducation,
        isEditingLocation: isEditingLocation,
        isEditingSkills: isEditingSkills,
        isEditingAbout: isEditingAbout,

        init: init,

        activate: activate,
        attached: attached,
        update: update,

        openDialogTest: openDialogTest
    }
});