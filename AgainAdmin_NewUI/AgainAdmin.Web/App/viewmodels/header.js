﻿define(['durandal/app', 'plugins/router', 'knockout',
        'viewmodels/profile', 'viewmodels/pages/app'],
function (app, router, ko, vmProfile) {

    var id = ko.observable();
    var fullName = ko.observable();
    var url = ko.observable();

    var init = function (data) {
        if (data != null) {
            id(data.id);
            fullName(data.fullName);
            
            if (data.userImage != null) {
                url(data.userImage.url);
            } else {
                url("../../Content/againadmin/img/user2-160x160.jpg");
            }
            
            // profile
            vmProfile.init(data);
        }
    };

    var attached = function () {
        _run_init();
    };

    var profile = function () {
        router.navigate("profile");
    };

    var signout = function () {
        $.cookie("token", "");
        router.navigate("");
        app.setRoot("viewmodels/login");
    };

    app.on("header.init", function(data) {
        init(data);
    });

    return {
        fullName: fullName,
        url: url,
            
        attached: attached,

        init: init,

        profile: profile,
        signout: signout
    }
});