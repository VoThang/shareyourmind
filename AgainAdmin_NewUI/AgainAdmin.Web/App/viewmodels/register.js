﻿define(['durandal/app', 'knockout', 'plugins/router',
    'services/auth', 'services/common', 'services/ko.validation.tooltip'],
function (app, ko, router,
    srvAuth, common) {

    var firstName = ko.observable();
    var lastName = ko.observable();
    var email = ko.observable();
    var password = ko.observable();
    var confirmPassword = ko.observable();

    var isProcessing = ko.observable(false);
    var isRegistered = ko.observable(false);
    var isExistEmail = ko.observable(false);

    // validation
    firstName.extend({
        required: true,
        maxLength: 35,
        validation: [{
            validator: function (val) {
                var regexp = new RegExp('^\S(.*\S)?$');

                if (regexp.test(val)) {
                    return false;
                }

                return true;
            },
            message: 'Space is not allowed at the beginning and end of the input'
        }]
    });

    lastName.extend({
        required: true,
        maxLength: 35,
        validation: [{
            validator: function (val) {
                var regexp = new RegExp('^\S(.*\S)?$');

                if (regexp.test(val)) {
                    return false;
                }

                return true;
            },
            message: 'Space is not allowed at the beginning and end of the input'
        }]
    });

    email.extend({
        required: true,
        maxLength: 80,
        validation: [{
            validator: function (val) {
                var regexp = new RegExp('^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$');

                if (regexp.test(val)) {
                    return true;
                }

                return false;
            },
            message: 'The field is wrong format'
        }]
    });

    password.extend({
        required: true,
        validation: [{
            validator: function (val) {
                val = val || "";

                if (val.length === 0) return true;

                var regexp = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;

                if (regexp.test(val)) {
                    return true;
                }

                return false;
            },
            message: 'Password should have at least 8 (eight) characters and include number, upper-case and lower-case letters'
        }]
    });

    confirmPassword.extend({
        required: true,
        validation: [{
            validator: function (val) {
                val = val || "";

                var pass = password() || "";

                if (pass.length > 0) {
                    if (val !== pass) {
                        return false;
                    }
                }

                return true;
            },
            message: 'Confirm password does not match the password'
        }]
    });

    var validationOptions = {
        decorateInputElement: true,
        insertMessages: false,
        errorsAsTooltip: true
    };

    var form = ko.validatedObservable({
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: password,
        confirmPassword: confirmPassword
    });

    var validate = function () {
        form.errors.showAllMessages();
        return form.isValid();
    };

    var resetValidation = function (val) {
        var fields = ['firstName', 'lastName', 'email', 'password', 'confirmPassword'];

        var f = form();
        ko.utils.arrayForEach(fields, function (item) {
            if (f.hasOwnProperty(item) && ko.isObservable(f[item])) {
                f[item].isModified(val);
            }
        });
    };

    var register = function () {
        if (isProcessing()) {
            return;
        }

        if (!validate()) {
            return;
        }

        var register = {
            firstName: firstName(),
            lastName: lastName(),
            email: email(),
            password: password(),
            confirmPassword: confirmPassword()
        }

        isProcessing(true);
        isRegistered(false);
        srvAuth.register(register).done(function(result) {
            if (result.status === "Success") {
                common.notification.success(result.message);
                isProcessing(false);
                isRegistered(true);
            } else {
                if (result.message === "Email already exist") {
                    common.notification.error(result.message);
                    isProcessing(false);
                    isExistEmail(true);
                }
            }
        }).fail(function() {
            isProcessing(false);
            common.notification.error("Internal server error");
        });
    };

    var reset = function () {
        isExistEmail(false);
        isRegistered(false);
        isProcessing(false);
        firstName("");
        lastName("");
        email("");
        password("");
        confirmPassword("");
    };

    var cancel = function () {
        reset();
        router.reset();
        router.deactivate();
        app.setRoot('viewmodels/login');
    };

    isProcessing.subscribe(function () {
        var element = $('.stateRegister');

        if (isProcessing()) {
            element.button('loading');
        } else {
            element.button('reset');
        }
    });

    isExistEmail.subscribe(function (val) {
        if (val) {
            email.setError("Email already exists");
            email.isModified(true);
        } else {
            email.clearError();
            email.isModified(false);
        }
    });

    var activate = function () {
        resetValidation(false);
    };

    return {
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: password,
        confirmPassword: confirmPassword,
        isExistEmail: isExistEmail,

        validationOptions: validationOptions,

        activate: activate,

        register: register,
        cancel: cancel,
        isRegistered: isRegistered
    };
});