﻿define(['durandal/app', 'plugins/router', 'services/user'],
function (app, router, srvUser) {
    

    var getUserInformation = function (email) {
        if (email == null) {
            router.reset();
            router.deactivate();
            app.setRoot('viewmodels/login');
            return;
        }

        var user = {
            email: email
        }
        srvUser.getUser(user).done(function (userResult) {
            app.trigger("header.init", userResult);
        });
    };

    return {
        getUserInformation: getUserInformation
    };
});