﻿define(['knockout', 'jquery'], function (ko, $) {

    var attached = function () {
        $('#gallery_grid ul').imagesLoaded(function () {
            // Prepare layout options.
            var options = {
                autoResize: true, // This will auto-update the layout when the browser window is resized.
                container: $('#gallery_grid'), // Optional, used for some extra CSS styling
                offset: 15, // Optional, the distance between grid items
                itemWidth: 220, // Optional, the width of a grid item (li)
                flexibleItemWidth: true

            };

            // Get a reference to your grid items.
            var handler = $('#gallery_grid ul li');

            // Call the layout function.
            handler.wookmark(options);

            $('#gallery_grid ul li').on('mouseenter', function () {
                $(this).addClass('act_tools');
            }).on('mouseleave', function () {
                $(this).removeClass('act_tools');
            });

        });
    };

    return {
        attached: attached
    }
});