﻿define(['jquery'],
function ($) {

    var attached = function() {
        $(".direct-chat-messages").slimscroll({
            height: $("direct-chat-messages").attr("height"),
            alwaysVisible: false,
            size: "3px"
        }).css("width", "100%");

        //$(".direct-chat-contacts").slimscroll({
        //    height: $("direct-chat-contacts").attr("height"),
        //    alwaysVisible: false,
        //    size: "3px"
        //}).css("width", "100%");
    };

    return {
      attached: attached      
    }
});