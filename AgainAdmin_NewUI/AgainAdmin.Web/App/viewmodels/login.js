﻿define(['durandal/app', 'knockout', 'services/auth',
        'jquery', 'plugins/router', 'services/user',
        'services/common',
        'services/ko.validation.tooltip'],
function (app, ko, srvAuth,
          $, router, srvUser,
          srvCommon) {

    var email = ko.observable();
    var password = ko.observable();

    var isProcessing = ko.observable();
    var isValidLogin = ko.observable(true);

    email.extend({
        required: true
    });

    password.extend({
        required: true
    });

    var validationOptions = {
        decorateInputElement: true,
        insertMessages: false,
        errorsAsTooltip: true
    };

    var form = ko.validatedObservable({
        email: email,
        password: password
    });

    var validate = function () {
        form.errors.showAllMessages();
        return form.isValid();
    };

    var resetValidation = function (val) {
        var fields = ['email', 'password'];

        var f = form();
        ko.utils.arrayForEach(fields, function (item) {
            if (f.hasOwnProperty(item) && ko.isObservable(f[item])) {
                f[item].isModified(val);
            }
        });
    };

    var login = function () {
        if (isProcessing()) {
            return;
        }

        if (!validate()) {
            return;
        }

        var user = {
            email: email(),
            password: password()
        }

        isProcessing(true);
        srvAuth.login(user).done(function (result) {
            isProcessing(false);

            if (result.access_token !== null && result.access_token.length > 0) {
                var date = new Date();
                var minutes = 30;
                date.setTime(date.getTime() + (minutes * 60 * 1000));
                $.cookie('token', result.access_token, { expires: date });

                var user = {
                    email: email()
                }
                srvUser.getUser(user).done(function(userResult) {
                    $.cookie('email', userResult.email, { expires: date });

                    app.trigger("header.init", userResult);

                    router.reset();
                    router.deactivate();
                    app.setRoot('viewmodels/shell');
                });
            } else {
                isValidLogin(false);
            }
        }).fail(function () {
            isProcessing(false);
            srvCommon.notification.error("Internal server error");
        });
    };

    var reset = function () {
        email("");
        password("");
        isValidLogin(true);
    };

    var signup = function () {
        reset();
        router.reset();
        router.deactivate();
        app.setRoot('viewmodels/register');
    };

    var forgotPassword = function () {
        reset();
        router.reset();
        router.deactivate();
        app.setRoot('viewmodels/register');
    };

    isProcessing.subscribe(function () {
        var element = $('.stateLogin');

        if (isProcessing()) {
            element.button('loading');
        } else {
            element.button('reset');
        }
    });

    var activate = function() {
        resetValidation(false);
    };

    var attached = function() {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    };

    return {
        email: email,
        password: password,
        isValidLogin: isValidLogin,

        validationOptions: validationOptions,

        login: login,
        signup: signup,
        forgotPassword: forgotPassword,

        activate: activate,
        attached: attached
    };
});