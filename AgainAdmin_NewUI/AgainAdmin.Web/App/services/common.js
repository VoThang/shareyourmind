﻿define(['jquery'], function ($) {
    var notification = function() {
        var success = function (content) {
            $('.top-right').notify({
                message: {
                     text: content
                },
                type: 'success',
                fadeOut: {
                    delay: Math.floor(Math.random() * 500) + 2500
                },
                closable: false
            }).show();
        };

        var error = function(content) {
            $('.top-right').notify({
                message: {
                    text: content
                },
                type: 'danger',
                fadeOut: {
                    delay: Math.floor(Math.random() * 500) + 2500
                },
                closable: false
            }).show();
        };

        var warning = function (content) {
            $('.top-right').notify({
                message: {
                    text: content
                },
                type: 'warning',
                fadeOut: {
                    delay: Math.floor(Math.random() * 500) + 2500
                },
                closable: false
            }).show();
        }

        var info = function(content) {
            $('.top-right').notify({
                message: {
                    text: content
                },
                type: 'info',
                fadeOut: {
                    delay: Math.floor(Math.random() * 500) + 2500
                },
                closable: false
            }).show();
        };

        return {
            success: success,
            error: error,
            warning: warning,
            info: info
        }
    }();

    var waitForMilisecond = function(milisecond) {
        var dfd = new $.Deferred();

        setTimeout(function() {
            dfd.resolve();
        }, milisecond);

        return dfd.promise();
    };

    var initDialogBootstrap = function(dialog) {
        dialog.addContext('bootstrap', {
            addHost: function(dialogInstance) {
                var body = $('body'),
                    host = $('<div class="modal fade" role="dialog"><div class="modal-dialog"><div class="modal-content"></div></div></div>');
                host.appendTo(body);
                dialogInstance.host = host.find('.modal-content').get(0);
                dialogInstance.modalHost = host;
            },
            removeHost: function(dialogInstance) {
                $(dialogInstance.modalHost).modal('hide');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
            },
            compositionComplete: function(child, parent, context) {
                var dialogInstance = dialog.getDialog(context.model),
                    $child = $(child);
                $(dialogInstance.modalHost).modal({ backdrop: 'static', keyboard: false, show: true });
                //Setting a short timeout is need in IE8, otherwise we could do this straight away
                setTimeout(function() {
                    $child.find('.autofocus').first().focus();
                }, 1);
                context.model.autoclose.subscribe(function(value) {
                    if (value) {
                        dialogInstance.close();
                    }
                });
            }
        });
        //rebind dialog.show to default to a new context
        var oldShow = dialog.show;
        dialog.show = function(obj, data, context) {
            return oldShow.call(dialog, obj, data, context || 'bootstrap');
        };
    };

    return {
        notification: notification,
        waitForMilisecond: waitForMilisecond,
        initDialogBootstrap: initDialogBootstrap
    }
});