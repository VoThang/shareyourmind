﻿define(['jquery'], function ($) {

    var getBlogs = function () {
        return $.ajax({
            url: "/AgainAdmin/GetBlogs",
            type: 'POST',
            cache: 'true',
            dataType: 'json',
            data: { token: $.cookie('token') }
        });
    };

    var getBlog = function (blog) {
        return $.ajax({
            url: "/AgainAdmin/GetBlog",
            type: 'POST',
            cache: 'true',
            dataType: 'json',
            data: { token: $.cookie('token'), blog: blog }
        });
    };

    var createBlog = function (blog) {
        return $.ajax({
            url: "/AgainAdmin/CreateBlog",
            type: 'POST',
            dataType: 'json',
            data: { token: $.cookie('token'), blog: blog }
        });
    };

    var updateBlog = function (blog) {
        return $.ajax({
            url: "/AgainAdmin/UpdateBlog",
            type: 'POST',
            dataType: 'json',
            data: { token: $.cookie('token'), blog: blog }
        });
    };

    var removeBlog = function (blog) {
        return $.ajax({
            url: "/AgainAdmin/RemoveBlog",
            type: 'POST',
            dataType: 'json',
            data: { token: $.cookie('token'), blog: blog }
        });
    };

    return {
        getBlogs: getBlogs,
        getBlog: getBlog,
        createBlog: createBlog,
        updateBlog: updateBlog,
        removeBlog: removeBlog
    }
});