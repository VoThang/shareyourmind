﻿define(['jquery'], function ($) {

    var getLikes = function (blog) {
        return $.ajax({
            url: "/AgainAdmin/GetLikes",
            type: 'POST',
            cache: 'true',
            dataType: 'json',
            data: { token: $.cookie('token'), blog: blog }
        });
    };

    var like = function (like) {
        return $.ajax({
            url: "/AgainAdmin/CreateLikes",
            type: "POST",
            dataType: "json",
            data: { token: $.cookie('token'), like: like }
        });
    };

    var updateLikes = function (likes) {
        return $.ajax({
            url: "/AgainAdmin/UpdateLikes",
            type: 'POST',
            dataType: 'json',
            data: { token: $.cookie('token'), likes: likes }
        });
    };

    return {
        getLikes: getLikes,
        like: like,
        updateLikes: updateLikes
    }
});