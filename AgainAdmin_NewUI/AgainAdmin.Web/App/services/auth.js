﻿define(['jquery', 'services/user', 'durandal/app'], function ($, srvUser, app) {

    var login = function(user) {
        return $.ajax({
            url: "/AgainAdmin/Login",
            type: 'POST',
            dataType: 'json',
            data: { user: user }
        });
    };

    var register = function(user) {
        var forgeryToken = $("#forgeryToken").val();

        return $.ajax({
            url: "/AgainAdmin/Register",
            type: 'POST',
            dataType: 'json',
            data: { user: user },
            headers: {
                '__RequestVerificationToken': forgeryToken
            }
        });
    };

    var getUserInformation = function (email) {
        if (email == null) {
            router.reset();
            router.deactivate();
            app.setRoot('viewmodels/login');
            return;
        }

        var user = {
            email: email
        }
        srvUser.getUser(user).done(function (userResult) {
            app.trigger("header.init", userResult);
        });
    };

    return {
        login: login,
        register: register,
        getUserInformation: getUserInformation
    }
});