﻿define(['jquery'], function ($) {

    var update = function (user) {
        return $.ajax({
            url: "/AgainAdmin/UpdateUser",
            type: 'POST',
            cache: 'true',
            dataType: 'json',
            data: { token: $.cookie('token'), user: user }
        });
    };

    return {
        update: update
    }
});