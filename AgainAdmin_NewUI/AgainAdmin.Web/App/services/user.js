﻿define(['jquery'], function ($) {

    var getUser = function (user) {
        return $.ajax({
            url: "/AgainAdmin/GetUser",
            type: 'POST',
            cache: 'true',
            dataType: 'json',
            data: { token: $.cookie('token'), user: user }
        });
    };

    var getUserImage = function(user) {
        return $.ajax({
            url: "/AgainAdmin/GetUser",
            type: 'POST',
            cache: 'true',
            dataType: 'json',
            data: { token: $.cookie('token'), user: user }
        });
    }

    return {
        getUser: getUser,
        getUserImage: getUserImage
    }
});