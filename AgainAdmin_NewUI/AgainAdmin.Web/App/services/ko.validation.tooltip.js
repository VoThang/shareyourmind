﻿/**
* Additional logic for Knockout validation
*
* New options:
*   - errorsAsTooltip: Enable displaying validation message as tooltip on the input element (default: false)
*   - tooltipShowInitially: Show tooltip when the input just become invalid (default: false, unless the input is focusing)
*   - formControlParentClass: When errorAsTooltiop is true, and an input having "form-control" validation state changes, 
*                               automatically find the input's closest ancestor having this class and add or remove 
*                               errorElementClass on it (default: "form-group", set to false to disable)
* 
* New bindings:
*   - validationMessageTooltip:
*       Display validation message as tooltip. Normally automatically applied on the input element when errorsAsTooltip is true.
*       But can also be manually applied to any element (e.g the <tr> in a custom <table> selector)
*
* See also:
* - https://github.com/Knockout-Contrib/Knockout-Validation/wiki/Configuration
* - https://github.com/Knockout-Contrib/Knockout-Validation
*/
define(['jquery', 'knockout'], function($, ko) {
    ko.bindingHandlers['validationMessageTooltip'] = { // individual error message, if modified or post binding
        update: function(element, valueAccessor) {
            var obsv = valueAccessor(),
                config = ko.validation.utils.getConfigOptions(element);

            if (obsv === null || typeof obsv === 'undefined') {
                throw new Error('Cannot bind validationMessageTooltip to undefined value. data-bind expression: ' +
                    element.getAttribute('data-bind'));
            }

            var isModified = obsv.isModified && obsv.isModified();
            var isValid = obsv.isValid && obsv.isValid();

            if (!config.messagesOnModified || isModified) {
                var error = isValid ? null : obsv.error();
                $(element).attr('title', error);
            }

            var shouldVisible = !config.messagesOnModified || isModified ? !isValid : false;
            var isCurrentlyVisible = $(element).data('bs.tooltip') != null || $(element).data('tooltip') != null;

            var tooltipTemplate = '<div class="tooltip tooltip-danger" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>';
            if (isCurrentlyVisible && !shouldVisible) {
                $(element).tooltip('destroy');
                
            } else if (!isCurrentlyVisible && shouldVisible) {
                $(element).tooltip({
                     template: tooltipTemplate
                });
                if (config.tooltipShowInitially || $(element).is(':focus')) 
                    $(element).tooltip('show');
                
            } else if (isCurrentlyVisible && shouldVisible) {
                $(element)
                    .tooltip('fixTitle')
                    .tooltip('show');
            }
            
            if ($(element).hasClass('form-control') && config.formControlParentClass !== false) {
                var formControlParentClass = config.formControlParentClass || 'form-group';
                var $formControlParentElement = $(element).closest('.' + formControlParentClass);
                if ($formControlParentElement.length) {
                    if (shouldVisible) $formControlParentElement.addClass(config.errorElementClass);
                    else $formControlParentElement.removeClass(config.errorElementClass);
                }
            }
        }
    };

    var validationCore = ko.bindingHandlers['validationCore'];
    ko.bindingHandlers['validationCore'] = {
        init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var config = ko.validation.utils.getConfigOptions(element);
            validationCore.init(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
            if (config.errorsAsTooltip) {
                var observable = valueAccessor();
                ko.applyBindingsToNode(element, { validationMessageTooltip: observable });
            }
        },
    };    
});
