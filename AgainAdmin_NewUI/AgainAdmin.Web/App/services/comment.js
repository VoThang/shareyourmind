﻿define(['jquery'], function ($) {

    var getComments = function (blog) {
        return $.ajax({
            url: "/AgainAdmin/GetComments",
            type: 'POST',
            cache: 'true',
            dataType: 'json',
            data: { token: $.cookie('token'), blog: blog }
        });
    };

    var comment = function (comment) {
        return $.ajax({
            url: "/AgainAdmin/CreateComment",
            type: "POST",
            dataType: "json",
            data: { token: $.cookie('token'), comment: comment }
        });
    };

    return {
        getComments: getComments,
        comment: comment
    }
});