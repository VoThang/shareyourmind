﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AgainAdmin.Web.Filter;
using AgainAdmin.Web.Hubs;
using AgainAdmin.Web.Utils;
using Autofac;
using Autofac.Integration.Mvc;

namespace AgainAdmin.Web.Module
{
    public class AgainAdminWebModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // Filters
            builder.RegisterFilterProvider();

            // Commom
            builder.RegisterType(typeof(Common)).SingleInstance();

            // SignalR 
            builder.RegisterType(typeof(AgainAdminManageConnection)).SingleInstance();
            builder.RegisterType<AgainAdminHub>().As<IAgainAdminHub>().SingleInstance();
        }
    }
}