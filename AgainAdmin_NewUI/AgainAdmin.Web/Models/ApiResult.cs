﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace AgainAdmin.Web.Models
{
    [JsonObject(MemberSerialization.OptIn)]
    public class GenerateTokenResult
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        [JsonProperty("expires_in")]
        public string ExpireIn { get; set; }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class UserResult : BaseModelResult
    {
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("fullName")]
        public string FullName { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("education")]
        public string Education { get; set; }

        [JsonProperty("location")]
        public string Location { get; set; }

        [JsonProperty("skills")]
        public string[] Skills { get; set; }

        [JsonProperty("about")]
        public string About { get; set; }

        [JsonProperty("userImage")]
        public UserImageResult UserImage { get; set; }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class RegisterResult : BaseModelResult
    {
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("confirmPassword")]
        public string ConfirmPassword { get; set; }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class BlogResult : BaseModelResult
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("content")]
        public string Content { get; set; }

        [JsonProperty("userId")]
        public string UserId { get; set; }

        [JsonProperty("createDate")]
        public string CreateDate { get; set; }

        [JsonProperty("updateDate")]
        public string UpdateDate { get; set; }

        [JsonProperty("isUpdate")]
        public bool IsUpdate { get; set; }

        [JsonProperty("likes")]
        public IEnumerable<LikesResult> Likes { get; set; }

        [JsonProperty("comments")]
        public IEnumerable<CommentResult> Comments { get; set; }

        [JsonProperty("userImage")]
        public UserImageResult UserImage { get; set; }

        [JsonProperty("user")]
        public UserResult User { get; set; }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class CommentResult : BaseModelResult
    {
        [JsonProperty("userId")]
        public string UserId { get; set; }

        [JsonProperty("blogId")]
        public string BlogId { get; set; }

        [JsonProperty("content")]
        public string Content { get; set; }

        [JsonProperty("createDate")]
        public string CreateDate { get; set; }

        [JsonProperty("userImage")]
        public UserImageResult UserImage { get; set; }

        [JsonProperty("user")]
        public UserResult User { get; set; }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class LikesResult : BaseModelResult
    {
        [JsonProperty("userId")]
        public string UserId { get; set; }

        [JsonProperty("blogId")]
        public string BlogId { get; set; }

        [JsonProperty("isLike")]
        public bool IsLike { get; set; }

        [JsonProperty("userImage")]
        public UserImageResult UserImage { get; set; }

        [JsonProperty("user")]
        public UserResult User { get; set; }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class UserImageResult : BaseModelResult
    {
        [JsonProperty("userId")]
        public string UserId { get; set; }

        [JsonProperty("fileNameInS3")]
        public string FileNameInS3 { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class BaseModelResult
    {
        [JsonProperty("id")]
        public string Id { get; set; }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public class ApiResult
    {
        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }
    }

   [JsonObject(MemberSerialization.OptIn)]
    public class HandleError : BaseModelResult
    {
        [JsonProperty("className")]
        public string ClassName { get; set; }

        [JsonProperty("method")]
        public string Method { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("stackTrace")]
        public string StackTrace { get; set; }

        [JsonProperty("dateTime")]
        public DateTime CreateTime { get; set; }
    }
}
