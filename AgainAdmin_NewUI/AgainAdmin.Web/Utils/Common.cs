﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgainAdmin.Web.Utils
{
    public class Common
    {
        public string GetHourMinutesNow()
        {
            return DateTime.Now.ToString("HH:mm");
        }
    }
}
