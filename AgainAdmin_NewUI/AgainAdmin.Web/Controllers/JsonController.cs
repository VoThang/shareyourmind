﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace AgainAdmin.Web.Controllers
{
    public class JsonController : Controller
    {
        private const string ContentType = "application/json";
        protected new ActionResult Json(object data, JsonRequestBehavior behavior = JsonRequestBehavior.DenyGet)
        {
            if (Request.RequestType == WebRequestMethods.Http.Get
                && behavior == JsonRequestBehavior.DenyGet)
                throw new Exception("Get is not allowed");
            var serializationSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
            var jsonResult = new ContentResult()
            {
                ContentType = ContentType,
                Content = JsonConvert.SerializeObject(data, serializationSettings)
            };
            return jsonResult;
        }
    }
}
