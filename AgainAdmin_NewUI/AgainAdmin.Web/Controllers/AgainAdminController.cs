﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AgainAdmin.Web.Filter;
using AgainAdmin.Web.Hubs;
using AgainAdmin.Web.Models;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Microsoft.SqlServer.Server;
using AgainAdmin.Framework.Logging;

namespace AgainAdmin.Web.Controllers
{
    public class AgainAdminController : JsonController
    {
        private readonly string _domain = ConfigurationManager.AppSettings["ApiDomain"];
        private readonly AgainAdminManageConnection _manageConnection;
        private readonly ILogger _logger;

        public AgainAdminController(AgainAdminManageConnection manageConnection, ILogger logger)
        {
            _manageConnection = manageConnection;
            _logger = logger;
        }

        public ActionResult Index()
        {
            return View("Index");
        }

        [HttpPost]
        [AjaxValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterResult user)
        {
            var result = new ApiResult()
            {
                Status = "",
                Message = ""
            };
            using (var client = GetHttpClient(_domain))
            {
                var postTask = client.PostAsJsonAsync("api/AgainAdmin/Register", user);

                var response = await postTask;

                if (response.IsSuccessStatusCode)
                {
                    var tokenResult = response.Content.ReadAsAsync<ApiResult>().Result;

                    return Json(tokenResult);
                }
            }

            return Json(result);
        }

        [HttpPost]
        public async Task<ActionResult> Login(RegisterResult user)
        {
            var result = new GenerateTokenResult()
            {
                AccessToken = "",
                TokenType = "",
                ExpireIn = ""
            };
            using (var client = GetHttpClient(_domain))
            {
                var content = new FormUrlEncodedContent(new[] 
                {
                    new KeyValuePair<string, string>("username", user.Email),
                    new KeyValuePair<string, string>("password", user.Password),
                    new KeyValuePair<string, string>("grant_type", "password")
                });
                var postTask = client.PostAsync("generatetoken", content);

                var response = await postTask;

                if (response.IsSuccessStatusCode)
                {
                    var tokenResult = response.Content.ReadAsAsync<GenerateTokenResult>().Result;

                    return Json(tokenResult);
                }
            }

            return Json(result);
        }

        [HttpPost]
        public async Task<ActionResult> GetUsers(string token)
        {
            var result = new List<UserResult>();
            try
            {
                using (var client = GetHttpClient(_domain))
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    var postTask = client.PostAsync("api/AgainAdmin/GetUsers", null);

                    var response = await postTask;

                    if (response.IsSuccessStatusCode)
                    {
                        var users = response.Content.ReadAsAsync<List<UserResult>>().Result;
                        foreach (var u in users)
                        {
                            var connectionId = _manageConnection.GetConnectonId(u.Email);
                            u.Status = !string.IsNullOrEmpty(connectionId) ? "online" : "offline";
                        }

                        return Json(users);
                    }
                }

                return Json(result);
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, e.StackTrace);
            }

            return Json(result);
        }

        [HttpPost]
        public async Task<ActionResult> GetUser(string token, UserResult user)
        {
            var result = new UserResult();
            try
            {
                using (var client = GetHttpClient(_domain))
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    var postTask = client.PostAsJsonAsync("api/AgainAdmin/GetUser", user);

                    var response = await postTask;

                    if (response.IsSuccessStatusCode)
                    {
                        result = response.Content.ReadAsAsync<UserResult>().Result;
                    }
                }
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, e.StackTrace);
            }

            return Json(result);
        }

        [HttpPost]
        public async Task<ActionResult> UpdateUser(string token, UserResult user)
        {
            var result = new ApiResult()
            {
                Status = "",
                Message = ""
            };
            try
            {
                using (var client = GetHttpClient(_domain))
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    var postTask = client.PostAsJsonAsync("api/AgainAdmin/UpdateUser", user);

                    var response = await postTask;

                    if (response.IsSuccessStatusCode)
                    {
                        result = response.Content.ReadAsAsync<ApiResult>().Result;
                    }
                }

                return Json(result);
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, e.StackTrace);
            }

            return Json(result);
        }

        [HttpPost]
        public async Task<ActionResult> GetBlogs(string token)
        {
            var result = new List<BlogResult>();
            try
            {
                using (var client = GetHttpClient(_domain))
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    var postTask = client.PostAsync("api/AgainAdmin/GetBlogs", null);

                    var response = await postTask;

                    if (response.IsSuccessStatusCode)
                    {
                        var blogs = response.Content.ReadAsAsync<List<BlogResult>>().Result;

                        return Json(blogs);
                    }
                }

                return Json(result);
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, e.StackTrace);
            }

            return Json(result);
        }

        [HttpPost]
        public async Task<ActionResult> GetBlog(string token, BlogResult blog)
        {
            var result = new BlogResult();
            try
            {
                using (var client = GetHttpClient(_domain))
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    var postTask = client.PostAsJsonAsync("api/AgainAdmin/GetBlog", blog);

                    var response = await postTask;

                    if (response.IsSuccessStatusCode)
                    {
                        result = response.Content.ReadAsAsync<BlogResult>().Result;
                    }
                }

                return Json(result);
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, e.StackTrace);
            }
            
            return Json(result);
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> CreateBlog(string token, BlogResult blog)
        {
            var result = new ApiResult()
            {
                Status = "",
                Message = ""
            };

            try
            {
                using (var client = GetHttpClient(_domain))
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    var postTask = client.PostAsJsonAsync("api/AgainAdmin/CreateBlog", blog);

                    var response = await postTask;

                    if (response.IsSuccessStatusCode)
                    {
                        result = response.Content.ReadAsAsync<ApiResult>().Result;
                    }
                }

                return Json(result);
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, e.StackTrace);
            }

            return Json(result);
        }

        [HttpPost]
        //[AjaxValidateAntiForgeryToken]
        public async Task<ActionResult> UpdateBlog(string token, BlogResult blog)
        {
            var result = new ApiResult()
            {
                Status = "",
                Message = ""
            };
            try
            {
                using (var client = GetHttpClient(_domain))
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    var postTask = client.PostAsJsonAsync("api/AgainAdmin/UpdateBlog", blog);

                    var response = await postTask;

                    if (response.IsSuccessStatusCode)
                    {
                        result = response.Content.ReadAsAsync<ApiResult>().Result;
                    }
                }

                return Json(result);
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, e.StackTrace);
            }

            return Json(result);
        }

        [HttpPost]
        public async Task<ActionResult> RemoveBlog(string token, BlogResult blog)
        {
            var result = new ApiResult()
            {
                Status = "",
                Message = ""
            };

            try
            {
                using (var client = GetHttpClient(_domain))
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    var postTask = client.PostAsJsonAsync("api/AgainAdmin/RemoveBlog", blog);

                    var response = await postTask;

                    if (response.IsSuccessStatusCode)
                    {
                        // remove all image on Amazon S3
                        result = response.Content.ReadAsAsync<ApiResult>().Result;
                    }
                }

                return Json(result);
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, e.StackTrace);
            }

            return Json(result);
        }



        [HttpPost]
        public async Task<ActionResult> GetComments(string token, BlogResult blog)
        {
            var result = new List<CommentResult>();
            try
            {
                using (var client = GetHttpClient(_domain))
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    var postTask = client.PostAsJsonAsync("api/AgainAdmin/GetComments", blog);

                    var response = await postTask;

                    if (response.IsSuccessStatusCode)
                    {
                        var comments = response.Content.ReadAsAsync<List<CommentResult>>().Result;

                        return Json(comments);
                    }
                }

                return Json(result);
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, e.StackTrace);
            }

            return Json(result);
        }

        [HttpPost]
        public async Task<ActionResult> CreateComment(string token, CommentResult comment)
        {
            var result = new LikesResult();

            try
            {
                using (var client = GetHttpClient(_domain))
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    var postTask = client.PostAsJsonAsync("api/AgainAdmin/CreateComment", comment);

                    var response = await postTask;

                    if (response.IsSuccessStatusCode)
                    {
                        result = response.Content.ReadAsAsync<LikesResult>().Result;
                    }
                }

                return Json(result);
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, e.StackTrace);
            }

            return Json(result);
        }

        [HttpPost]
        public async Task<ActionResult> GetLikes(string token, BlogResult blog)
        {
            var result = new List<LikesResult>();
            try
            {
                using (var client = GetHttpClient(_domain))
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    var postTask = client.PostAsJsonAsync("api/AgainAdmin/GetLikes", blog);

                    var response = await postTask;

                    if (response.IsSuccessStatusCode)
                    {
                        var comments = response.Content.ReadAsAsync<List<LikesResult>>().Result;

                        return Json(comments);
                    }
                }

                return Json(result);
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, e.StackTrace);
            }

            return Json(result);
        }

        [HttpPost]
        public async Task<ActionResult> CreateLikes(string token, LikesResult like)
        {
            var result = new List<LikesResult>();

            try
            {
                using (var client = GetHttpClient(_domain))
                {
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                    var postTask = client.PostAsJsonAsync("api/AgainAdmin/CreateLikes", like);

                    var response = await postTask;

                    if (response.IsSuccessStatusCode)
                    {
                        result = response.Content.ReadAsAsync<List<LikesResult>>().Result;
                    }
                }
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, e.StackTrace);
            }

            return Json(result);
        }

        [HttpPost]
        public async Task<ActionResult> UploadProfile(string token, string userId, string fileName)
        {
            try
            {
                if (Request.Files.Count > 0)
                {
                    var lstExtension = ConfigurationManager.AppSettings["ImageExtension"].Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();
                    var file = Request.Files["imageProfile"];
                    var maxlength = Convert.ToInt64(ConfigurationManager.AppSettings["ImageMaxLength"]);

                    if (file != null)
                    {
                        var extension = Path.GetExtension(file.FileName);
                        if (extension != null && lstExtension.Contains(extension.ToLower()))
                        {
                            if (file.ContentLength > maxlength)
                            {
                                throw new Exception("The maximum image size is 1MB");
                            }
                            else
                            {
                                var pathTemp = string.Format("{0}{1}{2}", ConfigurationManager.AppSettings["UploadPath"],
                                    "/Profile/", userId);
                                var name = string.Format("{0}_{1}", DateTime.Now.ToString("yyyyMMdHHss"), Path.GetFileName(file.FileName));
                                if (!Directory.Exists(Server.MapPath(pathTemp)))
                                {
                                    Directory.CreateDirectory(Server.MapPath(pathTemp));
                                }
                                var path = Path.Combine(Server.MapPath(pathTemp), name);
                                file.SaveAs(path);

                                // upload new image to cloud storage (Amazon S3)
                                var url = UpdateToAmazonS3(path, name, string.Format("{0}/{1}/", "Profile", userId));

                                // delete folder upload
                                Directory.Delete(pathTemp, true);

                                var userImageResult = new UserImageResult()
                                {
                                    UserId = userId,
                                    FileNameInS3 = name,
                                    Url = url
                                };

                                using (var client = GetHttpClient(_domain))
                                {
                                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                                    var postTask = client.PostAsJsonAsync("api/AgainAdmin/CreateUserImage", userImageResult);

                                    var response = await postTask;

                                    if (response.IsSuccessStatusCode)
                                    {
                                        return Json(new
                                        {
                                            urlS3 = url
                                        });
                                    }
                                }
                            }
                        }
                        else
                        {
                            throw new Exception(string.Format("{0} is not image (.gif,.png,.jpg,.jpeg)", file.FileName));
                        }
                    }
                }

                return Json(new
                {
                    urlS3 = string.Empty
                });
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, e.StackTrace);
            }

            return Json(false);
        }

        private string UpdateToAmazonS3(string localFilePath, string fileNameInS3, string subDirectoryInS3)
        {
            try
            {
                var s3Client = new AmazonS3Client(RegionEndpoint.APSoutheast1);
                var fileTransferUtility = new TransferUtility(s3Client);

                var fileTransferUtilityRequest = new TransferUtilityUploadRequest
                {
                    BucketName = ConfigurationManager.AppSettings["BucketName"],
                    FilePath = localFilePath,
                    StorageClass = S3StorageClass.ReducedRedundancy,
                    PartSize = 6291456, // 6 MB.The uploaded file will be divided into parts the size specified and uploaded to Amazon S3 individually
                    Key = fileNameInS3,
                    CannedACL = S3CannedACL.PublicRead
                };

                if (!string.IsNullOrEmpty(subDirectoryInS3))
                {
                    fileTransferUtilityRequest.BucketName = string.Format("{0}/{1}",
                        ConfigurationManager.AppSettings["BucketName"], subDirectoryInS3);
                }

                fileTransferUtility.Upload(fileTransferUtilityRequest);

                var requestUrl = new GetPreSignedUrlRequest()
                {
                    BucketName = ConfigurationManager.AppSettings["BucketName"],
                    Key = fileNameInS3,
                    Expires = DateTime.Now.AddMinutes(5)
                };

                if (!string.IsNullOrEmpty(subDirectoryInS3))
                {
                    requestUrl.BucketName = string.Format("{0}/{1}",
                        ConfigurationManager.AppSettings["BucketName"], subDirectoryInS3);
                }

                var url = s3Client.GetPreSignedURL(requestUrl);
                var uri = new Uri(url);
                var path = uri.GetLeftPart(UriPartial.Path);

                return path;
            }
            catch (AmazonS3Exception e)
            {
                throw new Exception("Exception in cloud storage: " + e.Message);
            }
        }

        private int DeleteFileOnAmazonS3(string subDirectoryInS3, IEnumerable<string> fileNamesInS3)
        {
            try
            {
                var deleteObjectRequest = new DeleteObjectsRequest
                {
                    BucketName = string.Format("{0}/{1}",
                        ConfigurationManager.AppSettings["BucketName"], subDirectoryInS3)
                };

                foreach (var item in fileNamesInS3)
                {
                    deleteObjectRequest.AddKey(item, null);
                }

                var s3Client = new AmazonS3Client(RegionEndpoint.APSoutheast1);
                var response = s3Client.DeleteObjects(deleteObjectRequest);

                return response.DeletedObjects.Count;
            }
            catch (AmazonS3Exception e)
            {
                throw new Exception("Exception in cloud storage: " + e.Message);
            }
        }

        private HttpClient GetHttpClient(string domain)
        {
            var client = new HttpClient()
            {
                BaseAddress = new Uri(domain)
            };

            return client;
        }
    }
}
