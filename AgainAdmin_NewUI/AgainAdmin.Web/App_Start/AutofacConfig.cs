﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using AgainAdmin.Framework;
using AgainAdmin.Web.Hubs;
using AgainAdmin.Web.Module;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.SignalR;
using Microsoft.AspNet.SignalR;

namespace AgainAdmin.Web.App_Start
{
    public class AutofacConfig
    {
        public static IContainer CreateContainer()
        {
            var builder = new ContainerBuilder();
            // Register controllers
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            // Register signalR
            builder.RegisterHubs(Assembly.GetExecutingAssembly());

            // Register mudules
            builder.RegisterModule<AutofacWebTypesModule>();
            builder.RegisterModule(new FrameworkModule());
            builder.RegisterModule(new AgainAdminWebModule());

            var container = builder.Build();

            DependencyResolver.SetResolver(new Autofac.Integration.Mvc.AutofacDependencyResolver(container));
            GlobalHost.DependencyResolver = new Autofac.Integration.SignalR.AutofacDependencyResolver(container);
            return container;
        }
    }
}