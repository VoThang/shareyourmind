using System;
using System.Web.Optimization;

namespace AgainAdmin.Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            // rem end line of file min js with this char /* */
            bundles.IgnoreList.Clear();
            AddDefaultIgnorePatterns(bundles.IgnoreList);

            bundles.Add(new StyleBundle("~/Content/css")
                .Include("~/Content/bootstrap.css")
                .Include("~/Content/againadmin/css/animate.min.css")
                .Include("~/Content/againadmin/css/skins/_all-skins.css")
                .Include("~/Content/againadmin/css/AdminLTE.css")
            );

            bundles.Add(
              new StyleBundle("~/libs/css")
                .Include("~/plugins/jvectormap/jquery-jvectormap-1.2.2.css")
                .Include("~/plugins/iCheck/square/blue.css")
                .Include("~/plugins/bootstrap-notify/css/bootstrap-notify.css")
                .Include("~/plugins/bootstrap-notify/css/alert-bangtidy.css")
              );

            bundles.Add(
             new ScriptBundle("~/scripts/vendor")
               .Include("~/Scripts/jquery-{version}.js")
               .Include("~/Scripts/knockout-{version}.js")
               .Include("~/Scripts/knockout.validation.js")
               .Include("~/Scripts/bootstrap.js")
               .Include("~/Scripts/toastr.min.js")
               .Include("~/Scripts/Q.min.js")
               .Include("~/Scripts/breeze.min.js")
               .Include("~/Scripts/moment.min.js")
               
                .Include("~/Scripts/againadmin/app.js")
                .Include("~/Scripts/againadmin/pages/dashboard2.js")
                .Include("~/Scripts/jquery.cookie.min.js")
                .Include("~/Scripts/jquery.ui.widget.js")
                .Include("~/Scripts/jquery.iframe-transport.js")
                .Include("~/Scripts/jquery.fileupload.js")

                .Include("~/plugins/fastclick/fastclick.min.js")
                .Include("~/plugins/sparkline/jquery.sparkline.min.js")
                .Include("~/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js")
                .Include("~/plugins/jvectormap/jquery-jvectormap-world-mill-en.js")
                .Include("~/plugins/slimScroll/jquery.slimscroll.min.js")
                .Include("~/plugins/chartjs/Chart.min.js")
                .Include("~/plugins/jquery.imagesloaded.min.js")
                .Include("~/plugins/jquery.wookmark.js")
                .Include("~/plugins/iCheck/icheck.min.js")
                .Include("~/plugins/bootstrap-notify/js/bootstrap-notify.js")
             );
        }

        public static void AddDefaultIgnorePatterns(IgnoreList ignoreList)
        {
            if (ignoreList == null)
            {
                throw new ArgumentNullException("ignoreList");
            }

            ignoreList.Ignore("*.intellisense.js");
            ignoreList.Ignore("*-vsdoc.js");
            ignoreList.Ignore("*.debug.js", OptimizationMode.WhenEnabled);
        }
    }
}